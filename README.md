## Installing Composer
https://getcomposer.org/download/
## Installing Laravel
composer global require "laravel/installer"
## Composer Create-Project
composer create-project --prefer-dist laravel/laravel report "5.4.*"
## Creating Auth
สร้างตัวlogin
php artisan make:auth
## Creating Controller
สร้างController
php artisan make:controller PhotoController --resource
php artisan make:controller ชื่อคอนโทรเลอ--resource
////ไฟล์ที่สร้างจะอยู่ใน C:\xampp\htdocs\ilockercontrol\app\Http\Controllers ////
## Creating Models
สร้างโมลเดท
php artisan make:model dBank
php artisan make:model ชื่อModel (table ในฐานข้อมูล)
////ไฟล์ที่สร้างจะอยู่ในC:\xampp\htdocs\ilockercontrol\app ////
## Generating Migrations
สร้างฐานข้มูล
php artisan make:migration create_users_table
php artisan make:migration create_ชื่อตาราง_table 
////ไฟล์ที่สร้างจะอยู่ในC:\xampp\htdocs\ilockercontrol\database\migrations ////
## Writing Seeders
สร้างข้อมูลในฐานข้อมูล
php artisan make:seeder UsersTableSeeder
php artisan make:seeder ชื่อตารางTableSeeder
////ไฟล์ที่สร้างจะอยู่ในC:\xampp\htdocs\ilockercontrol\database\seeds ////
## Running Seeders
รันคำสั่งทั้งหมดที่สร้างมาข้างบน
php artisan migrate:refresh --seed
## DB
Server:203.151.27.229 
user:devsk password:devsk55
DB:Gr_group
##Require Forms & HTML
https://laravelcollective.com/docs/5.4/html
##XDebug for Visual Studio Code
https://www.youtube.com/watch?v=eE6oxEhqqoU