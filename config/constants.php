<?php

return array(
    'SeedId' => array(
        'Default' => 0,
    ),
    'Company' => array(
        'ALLSUM' => array(
            'Id' => -1,
            'Name' => 'ทั้งหมดแบบรวม',
        ),
        'ALLSPLIT' => array(
            'Id' => 0,
            'Name' => 'ทั้งหมดแบบแยก',
        ),
        'FLOTECH' => array(
            'Id' => 1,
            'Name' => 'Flotech',
        ),
        'GREENSEEDS' => array(
            'Id' => 2,
            'Name' => 'GreenSeeds',
        ),
        'AVS' => array(
            'Id' => 8,
            'Name' => 'AVS',
        ),
    ),
);
