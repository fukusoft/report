var BASEPATH = window.location.protocol + "//" + window.location.host+"/report/";
var IMGLOAD = BASEPATH+'image/loading.gif';
var HTMLLOAD = '<div class="modal-dialog"><div class="modal-content"><div class="modal-header"></div><div class="modal-body"><center><img src="'+IMGLOAD+'"></center></div></div></div>';
$(document).ready(function(){
    getProduct();
});
function getProduct()
{
    //loading();
    document.getElementById("ddProduct").disabled = true;
    document.getElementById("btn-ok").disabled = true;
    var html ='<option value="-"> ------- กรุณารอสักครู่ ------ </option>';
    $("#ddProduct").html(html);
    var seed = document.getElementById("ddSeed");
    var seedId = seed.options[seed.selectedIndex].value;
    $.ajax({
       method: "GET",
       url: BASEPATH+'dd/product/'+seedId+'/ddtype/ajax',
       success: function (result) {
         html ='<option value="-"> ------- ทั้งหมด ------ </option>';
         result.forEach(function (data) {
             if(data.PdName!='-')
             {
                html +='<option value="'+data.idPd+'">'+data.PdName+'</option>';
             }
         });
         if(html=='')
         {
            html ='<option value="-"> ------- ทั้งหมด ------ </option>';
         }
         $("#ddProduct").html(html);
         document.getElementById("ddProduct").disabled = false;
         document.getElementById("btn-ok").disabled = false;
         //$('#loading').modal('toggle');
       },
       error: function (error) {
            html ='<option value="-"> ------- ทั้งหมด ------ </option>';
            $("#ddProduct").html(html);
            document.getElementById("ddProduct").disabled = false;
            document.getElementById("btn-ok").disabled = false;
       }
    });
}
function getProductTab(PdSelect)
{
    document.getElementById("ddProductTab").disabled = true;
    document.getElementById("btn-ok").disabled = true;
    var html ='<option value="-"> ------- กรุณารอสักครู่ ------ </option>';
    $("#ddProductTab").html(html);
    var seed = document.getElementById("ddSeedTab");
    var seedId = seed.options[seed.selectedIndex].value;
    $.ajax({
       method: "GET",
       url: BASEPATH+'dd/product/'+seedId+'/ddtype/ajax',
       success: function (result) {
         html ='<option value="-"> ------- ทั้งหมด ------ </option>';
         result.forEach(function (data) {
             if(data.PdName!='-')
             {
                if(PdSelect==data.idPd)
                {
                    html +='<option value="'+data.idPd+'" selected>'+data.PdName+'</option>';
                }
                else
                {
                    html +='<option value="'+data.idPd+'">'+data.PdName+'</option>';
                }
             }
         });
         if(html=='')
         {
            html ='<option value="-"> ------- ทั้งหมด ------ </option>';
         }
         $("#ddProductTab").html(html);
         document.getElementById("ddProductTab").disabled = false;
         document.getElementById("btn-ok").disabled = false;
       },
       error: function (error) {
            html ='<option value="-"> ------- ทั้งหมด ------ </option>';
            $("#ddProductTab").html(html);
            document.getElementById("ddProductTab").disabled = false;
            document.getElementById("btn-ok").disabled = false;
       }
    });
}
function searchReportStock()
{
    $('#searchReportStock').modal();
}
function pop(id)
{
    $('#pop'+id).modal();
}
function PdEstPC(PdName,PlantName,EsDate,idPd,ReportType,idComp)
{
    var html = HTMLLOAD;
    $("#PdEstHavest").html(html);
    $('#PdEstPC').modal();
    $("#PdEstPCPdName").html(PdName);
    $("#PdEstPCPlantName").html(PlantName);
    $("#PdEstPCEsDate").html(EsDate+' วัน');
    $url = BASEPATH+'PdEstPC/ProductName/'+PdName+'/PlantName/'+PlantName+'/ProductId/'+idPd+'/Company/'+idComp+'/EsDate/'+EsDate;
    $.ajax({
        method: "GET",
        url: $url,
        success: function (html) {
            $("#PdEstPC").html(html);
        },
        error: function (error) {
            alert("Error!!");
        }
     });

}
function PdEstHavest(PdName,PlantName,EsDate,idPd,ReportType,idComp)
{
    var sort_by=$('input[name="PdEstHavest_sort_by"]:checked').val();
    var sort_type=$('input[name="PdEstHavest_sort_type"]:checked').val();
    var list_type=$('input[name="PdEstHavest_list_type"]:checked').val();
    var html = HTMLLOAD;
    $("#PdEstHavest").html(html);
    $('#PdEstHavest').modal();
    $("#PdEstHavestPdName").html(PdName);
    $("#PdEstHavestPlantName").html(PlantName);
    $("#PdEstHavestEsDate").html(EsDate+' วัน');
    $url = BASEPATH+'PdEstHavest/ProductName/'+PdName+'/PlantName/'+PlantName+'/ProductId/'+idPd+'/Company/'+idComp+'/EsDate/'+EsDate+'/SortBy/'+sort_by+'/SortType/'+sort_type+'/ListType/'+list_type;
    $.ajax({
        method: "GET",
        url: $url,
        success: function (html) {
            $("#PdEstHavest").html(html);
        },
        error: function (error) {
            alert("Error!!");
        }
     });
}
function PdLotMixList(PdName,PlantName,Weight,idPd,ReportType,idComp)
{
    LotCode = $('#LotCode').val()!== undefined ? $('#LotCode').val() : null;
    Grad = $('#Grad').val()!== undefined ? $('#Grad').val() : null;
    txtGrowS=$('#txtGrowS').val()!== undefined ? $('#txtGrowS').val() : null;
    txtGrowE=$('#txtGrowE').val()!== undefined ? $('#txtGrowE').val() : null;
    txtHumidS=$('#txtHumidS').val()!== undefined ? $('#txtHumidS').val() : null;
    txtHumidE=$('#txtHumidE').val()!== undefined ? $('#txtHumidE').val() : null;
    txtPureS=$('#txtPureS').val()!== undefined ? $('#txtPureS').val() : null;
    txtPureE=$('#txtPureE').val()!== undefined ? $('#txtPureE').val() : null;
    var html = HTMLLOAD;
    $("#PdLotMixList").html(html);
    $('#PdLotMixList').modal();
    $url = BASEPATH+'PdLotMixList';
    data = {
            'ProductName':PdName,
            'PlantName':PlantName,
            'ProductId':idPd,
            'Company':idComp,
            'Weight':Weight,
            'LotCode':LotCode,
            'Grad':Grad,
            'txtGrowS':txtGrowS,
            'txtGrowE':txtGrowE,
            'txtHumidS':txtHumidS,
            'txtHumidE':txtHumidE,
            'txtPureS':txtPureS,
            'txtPureE':txtPureE
           };
    $.ajax({
        method: "GET",
        data:data,
        url: $url,
        success: function (html) {
            $("#PdLotMixList").html(html);
        },
        error: function (error) {
            console.log(error);
        }
     });
}
function PdStock(Tab,PdName,PlantName,Weight,idPd,ReportType,idComp)
{
    ChkNoPd= $('#ChkNoPd').is(':checked');
    LotCodeRef = $('#LotCodeRef').val()!== undefined ? $('#LotCodeRef').val() : null;
    LotNum = $('#LotNum').val()!== undefined ? $('#LotNum').val() : null;
    Grad = $('#Grad').val()!== undefined ? $('#Grad').val() : null;
    txtGrowS=$('#txtGrowS').val()!== undefined ? $('#txtGrowS').val() : null;
    txtGrowE=$('#txtGrowE').val()!== undefined ? $('#txtGrowE').val() : null;
    txtHumidS=$('#txtHumidS').val()!== undefined ? $('#txtHumidS').val() : null;
    txtHumidE=$('#txtHumidE').val()!== undefined ? $('#txtHumidE').val() : null;
    txtPureS=$('#txtPureS').val()!== undefined ? $('#txtPureS').val() : null;
    txtPureE=$('#txtPureE').val()!== undefined ? $('#txtPureE').val() : null;
    idSeed=$('select[name=ddSeedTab]').val()!== undefined ? $('select[name=ddSeedTab]').val() : '-';
    idProduct=$('select[name=ddProductTab]').val()!== undefined ? $('select[name=ddProductTab]').val() : '-';
    Chk=$(".CHK").find(":checked").val()!== undefined ? $(".CHK").find(":checked").val() : 'CHKFixMinMax';
    var html = HTMLLOAD;
    $("#PdStock").html(html);
    $('#PdStock').modal();
    $url = BASEPATH+'PdStock';
    data = {
            'Tab':Tab,
            'ProductName':PdName,
            'PlantName':PlantName,
            'ProductId':idPd,
            'Company':idComp,
            'Weight':Weight,
            'LotCodeRef':LotCodeRef,
            'LotNum':LotNum,
            'Grad':Grad,
            'txtGrowS':txtGrowS,
            'txtGrowE':txtGrowE,
            'txtHumidS':txtHumidS,
            'txtHumidE':txtHumidE,
            'txtPureS':txtPureS,
            'txtPureE':txtPureE,
            'ChkNoPd':ChkNoPd,
            'idSeed':idSeed,
            'idProduct':idProduct,
            'Chk':Chk
           };
    $.ajax({
        method: "GET",
        data:data,
        url: $url,
        success: function (html) {
            $("#PdStock").html(html);
            getProductTab(idProduct);
            if(Tab=="Tab1")
            {
                $( "#tab-stock" ).attr( "class", "active " );
                $( "#tab-item" ).attr( "class", "" );
                $( "#stock" ).attr( "class", "active tab-pane" );
                $( "#item" ).attr( "class", "tab-pane" );
            }
            else
            {
                $( "#tab-stock" ).attr( "class", "" );
                $( "#tab-item" ).attr( "class", "active" );
                $( "#stock" ).attr( "class", "tab-pane" );
                $( "#item" ).attr( "class", "active tab-pane" );
            }
        },
        error: function (error) {
            console.log(error);
        }
     });
}
function loading()
{
    $('#loading').modal();
}