@extends('reports.stock.rp_stock')
@section('sub_content')
<div class="table-responsive">
    <table class="tree table table-bordered">
        <thead>            
            <tr bgcolor="#A9E2F3">
                <th style="width: 10px">ลำดับ</th>
                <th width="40%">พืช</th>
                <th>ประมาณการรอเก็บเกี่ยวจากสส</th>
                <th>ประมาณการรอเก็บเกี่ยวจากสูตร</th>
                <th>ประมาณวันรอเก็บเกี่ยว</th>
                <th>เมล๊ดระหว่างกระบวนการในคลัง</th>
                <th>ประมาณวันเสร็จกระบวนการในคลัง</th>
                <th>สำเร็จรูปพร้อมขาย</th>
                <th>ประมาณน้ำหนักทั้งหมด</th>
            </tr>
        </thead>
        <tbody>
        @php
            $i=0;
            $ckProductName="";
            $ckPlantName="";
        @endphp
        @foreach ($Results as $PlantName=>$result)
            @php
                $i++;
            @endphp
            @if($ckPlantName=="")
                @php
                    $ckPlantName=$PlantName
                @endphp
            @elseif($ckPlantName!=$PlantName)
                @php
                    $ckPlantName=$PlantName
                @endphp
            @endif
            <tr data-level="0" bgcolor="#E6E6FA">
                <td>{{ $i }}</td>
                <td colspan="7">{{ $PlantName }}</td>
                <td></td>
            </tr>
            @foreach ($result as $ProductName=>$data)
                @if($ckProductName=="")
                    @php
                        $ckProductName=$ProductName
                    @endphp
                @elseif($ckProductName!=$ProductName)
                    @php
                        $ckProductName=$ProductName
                    @endphp
                @endif
                <tr data-level="0" bgcolor="#E0F2F7">
                    <td></td>
                    <td>{{ $ProductName }}</td>
                    <td colspan="7"></td>
                </tr>
                @foreach ($data as $PdName=>$items)
                @php
                    $Flotech_S6 = isset($items['Flotech']['S6']) ? $items['Flotech']['S6'] : 0;
                    $Flotech_BalancePc = isset($items['Flotech']['BalancePc']) ? $items['Flotech']['BalancePc'] : 0;
                    $Flotech_PsEstimate = isset($items['Flotech']['PsEstimate']) ? $items['Flotech']['PsEstimate'] : 0;
                    $GreenSeeds_S6 = isset($items['GreenSeeds']['S6']) ? $items['GreenSeeds']['S6'] : 0;
                    $GreenSeeds_BalancePc = isset($items['GreenSeeds']['BalancePc']) ? $items['GreenSeeds']['BalancePc'] : 0;
                    $GreenSeeds_PsEstimate = isset($items['GreenSeeds']['PsEstimate']) ? $items['GreenSeeds']['PsEstimate'] : 0;
                    $sumPsEstimate=$Flotech_PsEstimate+$GreenSeeds_PsEstimate;
                    $sumS6=$Flotech_S6+$GreenSeeds_S6;
                    $sumBalancePc=$Flotech_BalancePc+$GreenSeeds_BalancePc;
                    if (array_key_exists("Flotech",$items))
                    {
                        $AvStock=$items['Flotech']['AvStock'];
                    }
                    else
                    {
                        $AvStock=$items['GreenSeeds']['AvStock'];
                    }
                    $SumWeight=$AvStock+$sumS6+$sumBalancePc+$sumPsEstimate;
                @endphp
                <tr data-level="1" bgcolor="#E0E0F8">
                    <td></td>
                    <td colspan="7">{{ $PdName }}</td>
                    <td align="right">{{ number_format($SumWeight,2) }}</td>
                </tr>
                @foreach ($items as $Company=>$item)
                    @php
                        $PsEstimate=$item['PsEstimate'];
                        $EstimateW=$item['EstimateW'];
                        $AvStock=$item['AvStock'];
                        $BalancePc=$item['BalancePc'];
                        $S6=$item['S6'];
                        $PdName=$item['PdName'];
                        $PlantName=$item['PlantName'];
                        $SumWeight=$BalancePc+$S6+$PsEstimate;
                        $EsDate=$item['EsDate'];
                        $EPDate=$item['EPDate'];
                        $idPd=$item['idPd'];
                        $idCompb=$item['idCompb'];
                    @endphp
                    <tr data-level="2">
                        <td></td>
                        <td>{{ $Company }}</td>
                        <td align="right">
                        @if($PsEstimate!=0)
                            <a onclick="PdEstPC('{{ $PdName }}','{{ $PlantName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($PsEstimate,2) }}</a>
                        @else
                            {{ number_format($PsEstimate,2) }}
                        @endif
                        </td>
                        <td align="right">
                        @if($EstimateW!=0)
                            <a onclick="PdEstPC('{{ $PdName }}','{{ $PlantName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($EstimateW,2) }}</a>
                        @else
                            {{ number_format($EstimateW,2) }}
                        @endif
                        </td>
                        <td align="center">{{$EsDate}}</td>
                        <td align="right">
                        @if($BalancePc!=0)
                            <a onclick="PdEstHavest('{{ $PdName }}','{{ $PlantName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($BalancePc,2) }}</a>
                        @else
                            {{ number_format($BalancePc,2) }}
                        @endif
                        </td>
                        <td align="center">{{ $EPDate }}</td>
                        <td align="right">
                        @if($S6!=0)
                            <a onclick="PdLotMixList('{{ $PdName }}','{{ $PlantName }}','{{ $S6 }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($S6,2) }}</a>
                        @else
                            {{ number_format($S6,2) }}
                        @endif
                        </td>
                        <td align="right">{{ number_format($SumWeight,2) }}</td>
                    </tr>
                @endforeach
                <tr data-level="2">
                    <td></td>
                    <td>AVS</td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right">{{ number_format($AvStock,2) }}</td>
                </tr>                    
                @endforeach
            @endforeach
        @endforeach
        </tbody> 
    </table>
</div>
@stop