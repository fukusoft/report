@extends('reports.stock.rp_stock')
@section('sub_content')
<div class="table-responsive">
    <table class="tree table table-bordered">           
        <thead>
            <tr bgcolor="#A9E2F3">
                <th style="width: 10px">ลำดับ</th>
                <th width="25%">สินค้า</th>
                <th width="15%">พืช</th>
                <th>ประมาณรอเก็บเกี่ยวสส.</th>
                <th>ประมาณรอเก็บเกี่ยวจากสูตร</th>
                <th>ประมาณวันรอเก็บเกี่ยว</th>
                <th>เมล็ดระหว่างกระบวนการ</th>
                <th>ประมาณวันเสร็จกระบวนการในคลัง</th>
                <th>คลังสำเร็จรูป</th>
                <th>ประมาณน้ำหนักทั้งหมด</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i=0;
            @endphp
            @foreach ($Results as $data)
                @php
                    $i++;
                    $PsEstimate=0;
                    $EstimateW=0;
                    $BalancePc=0;
                    $AvStock=0;
                    $S6=0;
                    $SumWeight=0;
                    $PdName="";
                    $PlantName="";
                @endphp
                @foreach ($data as $Company=>$item)
                    @php
                        $PsEstimate+=$item['PsEstimate'];
                        $EstimateW+=$item['EstimateW'];
                        $AvStock=$item['AvStock'];
                        $BalancePc+=$item['BalancePc'];
                        $S6+=$item['S6'];
                        $PdName=$item['PdName'];
                        $PlantName=$item['PlantName'];
                        $EsDate=$item['EsDate'];
                        $EPDate=$item['EPDate'];
                        $idPd=$item['idPd'];
                        @endphp
                @endforeach
                @php
                    $SumWeight=$AvStock+$BalancePc+$S6+$PsEstimate;
                @endphp
                <tr data-level="0">
                    <td>{{ $i }}</td>
                    <td>{{ $PdName }}</td>
                    <td>{{ $PlantName }}</td>
                    <td align="right">
                        @if($PsEstimate!=0)
                            <a onclick="PdEstPC('{{ $PdName }}','{{ $PlantName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},0)">{{ number_format($PsEstimate,2) }}</a>
                        @else
                            {{ number_format($PsEstimate,2) }}
                        @endif
                    </td>
                    <td align="right">
                        @if($EstimateW!=0)
                            <a onclick="PdEstPC('{{ $PdName }}','{{ $PlantName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},0)">{{ number_format($EstimateW,2) }}</a>
                        @else
                            {{ number_format($EstimateW,2) }}
                        @endif
                    </td>
                    <td align="center">{{ $EsDate }}</td>
                    <td align="right">
                        @if($BalancePc!=0)
                            <a onclick="PdEstHavest('{{ $PdName }}','{{ $PlantName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},0)">{{ number_format($BalancePc,2) }}</a>
                        @else
                            {{ number_format($BalancePc,2) }}
                        @endif
                    </td>
                    <td align="center">{{ $EPDate }}</td>
                    <td align="right">{{ number_format(($AvStock+$S6),2) }}</td>
                    <td align="right">{{ number_format($SumWeight,2) }}</td>
                </tr>
            @endforeach
        </tbody> 
    </table>
</div>
@stop