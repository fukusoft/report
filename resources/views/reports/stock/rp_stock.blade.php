@extends('layouts.main')
@section('content')
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <a onclick="searchReportStock()" class="btn btn-block btn-social btn-bitbucket" >
                    <i class="fa fa-search"></i> ค้นหา
                </a>
            </h3>
        </div>
        <div class="box-body">
        @yield('sub_content')
        </div>
    </div>
</div>
<div id="searchReportStock" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ค้นหาข้อมูล</h4>
      </div>
      {!! Form::open(['url' => 'stock', 'method' => 'post', 'id'=>'form']) !!}
      <div class="modal-body">
            <div class="form-group">
                <label for="ddSeed">ค้นหาจากพืช</label>
                {{ Form::select('ddSeed', $ddSeed,$ddSeed,['class' => 'form-control','id'=>'ddSeed','onchange'=> 'getProduct()']) }}
            </div>
            <div class="form-group">
                <label for="ddProduct">สินค้า</label>
                {{ Form::select('ddProduct', $ddProduct,$ddProduct,['class' => 'form-control','id'=>'ddProduct']) }}
            </div>
            <div class="form-group">
                <label for="ddCompany">คลังบริษัท</label>
                {{ Form::select('ddCompany', $ddCompany,$ddCompany,['class' => 'form-control','id'=>'ddCompany']) }}
            </div>
            <div class="form-group">
                <label for="ddOrderBy">การจัดเรียง</label>
                {{ Form::select('ddOrderBy', $ddOrderBy,$ddOrderBy,['class' => 'form-control','id'=>'ddOrderBy']) }}
            </div>
      </div>
      <div class="modal-footer">
         {!! csrf_field() !!}
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="return false;">ยกเลิก</button>
        <button type="submit" class="btn btn-primary btn-ok" data-dismiss="modal" onclick="document.getElementById('form').submit();loading()" id="btn-ok">ค้นหา</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>
<div id="PdEstPC" class="modal fade in" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <center><img src="{!! asset('image/loading.gif') !!}"></center>
        </div>
        </div>
    </div>    
</div>
<div id="PdEstHavest" class="modal fade in" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <center><img src="{!! asset('image/loading.gif') !!}"></center>
        </div>
        </div>
    </div>       
</div>
<div id="PdLotMixList" class="modal fade in" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <center><img src="{!! asset('image/loading.gif') !!}"></center>
        </div>
        </div>
    </div>        
</div>
<div id="PdStock" class="modal fade in" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <center><img src="{!! asset('image/loading.gif') !!}"></center>
        </div>
        </div>
    </div>        
</div>
<div id="loading" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      </div>
      <div class="modal-body">
        <center><img src="{!! asset('image/loading.gif') !!}"></center>
      </div>
    </div>
  </div>
</div>
@stop