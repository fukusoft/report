@extends('reports.stock.rp_stock')
@section('sub_content')
<div class="table-responsive">
    <table class="tree table table-bordered">
        <thead>            
            <tr bgcolor="#A9E2F3">
                <th style="width: 10px">ลำดับ</th>
                <th width="40%">พืช</th>
                <th>ประมาณการรอเก็บเกี่ยวจากสส</th>
                <th>ประมาณการรอเก็บเกี่ยวจากสูตร</th>
                <th>ประมาณวันรอเก็บเกี่ยว</th>
                <th>เมล๊ดระหว่างกระบวนการในคลัง</th>
                <th>ประมาณวันเสร็จกระบวนการในคลัง</th>
                <th>สำเร็จรูปพร้อมขาย</th>
                <th>ประมาณน้ำหนักทั้งหมด</th>
            </tr>
        </thead>
        <tbody>
        @php
            $i=0;
            $ckProductName="";
            $ckPlantName="";
        @endphp
        @foreach ($Results as $PlantName=>$result)
            @php
                $i++;
            @endphp
            @if($ckPlantName=="")
                @php
                    $ckPlantName=$PlantName
                @endphp
            @elseif($ckPlantName!=$PlantName)
                @php
                    $ckPlantName=$PlantName
                @endphp
            @endif
            <tr data-level="0" bgcolor="#E6E6FA">
                <td>{{ $i }}</td>
                <td colspan="7">{{ $PlantName }}</td>
                <td></td>
            </tr>
            @foreach ($result as $ProductName=>$data)
                @if($ckProductName=="")
                    @php
                        $ckProductName=$ProductName
                    @endphp
                @elseif($ckProductName!=$ProductName)
                    @php
                        $ckProductName=$ProductName
                    @endphp
                @endif
                <tr data-level="0" bgcolor="#E0F2F7">
                    <td></td>
                    <td>{{ $ProductName }}</td>
                    <td colspan="7"></td>
                </tr>
                @foreach ($data as $PdName=>$items)
                    @php
                        $PsEstimate=0;
                        $EstimateW=0;
                        $BalancePc=0;
                        $AvStock=0;
                        $S6=0;
                        $SumWeight=0;
                        $PlantName="";
                    @endphp
                    @foreach ($items as $Company=>$item)
                        @php
                            $PsEstimate+=$item['PsEstimate'];
                            $EstimateW+=$item['EstimateW'];
                            $AvStock+=$item['AvStock'];
                            $BalancePc+=$item['BalancePc'];
                            $S6+=$item['S6'];
                            $PdName=$item['PdName'];
                            $PlantName=$item['PlantName'];
                            $SumWeight+=$BalancePc+$S6+$PsEstimate;
                            $EsDate=$item['EsDate'];
                            $EPDate=$item['EPDate'];
                            $idPd=$item['idPd'];
                            $idCompb=$item['idCompb'];
                        @endphp
                    @endforeach
                    <tr data-level="1" bgcolor="#E0E0F8">
                        <td></td>
                        <td>{{ $PdName }}</td>
                        <td align="right">
                        @if($PsEstimate!=0)
                            <a onclick="PdEstPC('{{ $PdName }}','{{ $PlantName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($PsEstimate,2) }}</a>
                        @else
                            {{ number_format($PsEstimate,2) }}
                        @endif
                        </td>
                        <td align="right">
                        @if($EstimateW!=0)
                            <a onclick="PdEstPC('{{ $PdName }}','{{ $PlantName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($EstimateW,2) }}</a>
                        @else
                            {{ number_format($EstimateW,2) }}
                        @endif
                        </td>
                        <td align="center">{{ $EsDate }}</td>
                        <td align="right">
                        @if($BalancePc!=0)
                        <a onclick="PdEstHavest('{{ $PdName }}','{{ $PlantName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($BalancePc,2) }}</a>
                        @else
                            {{ number_format($BalancePc,2) }}
                        @endif
                        </td>
                        <td align="center">{{ $EPDate }}</td>
                        <td align="right">
                        @if($S6!=0)
                            @if($ReportType!=8)
                            <a onclick="PdLotMixList('{{ $PdName }}','{{ $PlantName }}','{{ $S6 }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($S6,2) }}</a>
                            @else
                            <a onclick="PdStock('Tab1','{{ $PdName }}','{{ $PlantName }}','{{ $S6 }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($S6,2) }}</a>
                            @endif
                        @else
                            {{ number_format($S6,2) }}
                        @endif
                        </td>
                        <td align="right">{{ number_format($SumWeight,2) }}</td>
                    </tr>                    
                @endforeach
            @endforeach
        @endforeach
        </tbody> 
    </table>
</div>
@stop