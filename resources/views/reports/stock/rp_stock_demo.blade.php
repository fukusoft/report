@extends('layouts.main')
@section('content')
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <!--h3 class="box-title">
                <a onclick="searchReportStock()" class="btn btn-block btn-social btn-bitbucket" >
                    <i class="fa fa-search"></i> ค้นหา
                </a>
            </h3-->
        </div>
        <div class="box-body">
            <!--table class="tree table table-bordered table-striped">
                <tr>
                    <th style="width: 10px">ลำดับ</th>
                    <th width="20%">พืช</th>
                    <th>ประมาณรอเก็บสส</th>
                    <th>ประมาณรอเก็บจากสูตร</th>
                    <th>ประมาณรอเก็บเกี่ยว</th>
                    <th>เมล็ดระหว่างกระบวนการ</th>
                    <th>ประมาณวันเสร็จกระบวนการในคลัง</th>
                    <th>คลังสำเร็จรูป</th>
                    <th>ประมาณน้ำหนักทั้งหมด</th>
                </tr>
                <tr data-level="0">
                    <td>1</td>
                    <td>ข้าวโพดหวาน</td>
                    <td align="right">300</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>ข้าวโพดหวาน Sun06</td>
                    <td align="right">200</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="2">
                    <td></td>
                    <td>-Flo</td>
                    <td align="right">50</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="2">
                    <td></td>
                    <td>-Green</td>
                    <td align="right">100</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="2">
                    <td></td>
                    <td>-AVS</td>
                    <td align="right">50</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>ข้าวโพดหวาน Sun09</td>
                    <td align="right">100</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="2">
                    <td></td>
                    <td>-Flo</td>
                    <td align="right">20</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="2">
                    <td></td>
                    <td>-Green</td>
                    <td align="right">50</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="2">
                    <td></td>
                    <td>-AVS</td>
                    <td align="right">30</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
            </table>
            <table class="tree table table-bordered table-striped">
                <tr>
                    <th style="width: 10px">ลำดับ</th>
                    <th width="20%">พืช</th>
                    <th>ประมาณรอเก็บสส</th>
                    <th>ประมาณรอเก็บจากสูตร</th>
                    <th>ประมาณรอเก็บเกี่ยว</th>
                    <th>เมล็ดระหว่างกระบวนการ</th>
                    <th>ประมาณวันเสร็จกระบวนการในคลัง</th>
                    <th>คลังสำเร็จรูป</th>
                    <th>ประมาณน้ำหนักทั้งหมด</th>
                </tr>
                <tr data-level="0">
                    <td>1</td>
                    <td>ข้าวโพดหวาน</td>
                    <td align="right">300</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>ข้าวโพดหวาน Sun06</td>
                    <td align="right">200</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>ข้าวโพดหวาน Sun09</td>
                    <td align="right">100</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
            </table>
            <table class="tree table table-bordered table-striped">
                <tr>
                    <th style="width: 10px">ลำดับ</th>
                    <th width="20%">พืช</th>
                    <th>ประมาณรอเก็บสส</th>
                    <th>ประมาณรอเก็บจากสูตร</th>
                    <th>ประมาณรอเก็บเกี่ยว</th>
                    <th>เมล็ดระหว่างกระบวนการ</th>
                    <th>ประมาณวันเสร็จกระบวนการในคลัง</th>
                    <th>คลังสำเร็จรูป</th>
                    <th>ประมาณน้ำหนักทั้งหมด</th>
                </tr>
                <tr data-level="0">
                    <td>1</td>
                    <td>ข้าวโพดหวาน Sun06</td>
                    <td align="right">300</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">100</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>-Flo</td>
                    <td align="right">50</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">10</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>-Green</td>
                    <td align="right">100</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">50</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>-AVS</td>
                    <td align="right">50</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">40</td>
                </tr>
                <tr data-level="0">
                    <td>2</td>
                    <td>แตงโม</td>
                    <td align="right">200</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">90</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>-Flo</td>
                    <td align="right">50</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">20</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>-Green</td>
                    <td align="right">100</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">20</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>-AVS</td>
                    <td align="right">50</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">50</td>
                </tr>
                <tr data-level="0">
                    <td>3</td>
                    <td>ข้าวโพดหวาน Sun09</td>
                    <td align="right">100</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">60</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>-Flo</td>
                    <td align="right">50</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>-Green</td>
                    <td align="right">100</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">40</td>
                </tr>
                <tr data-level="1">
                    <td></td>
                    <td>-AVS</td>
                    <td align="right">50</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">0</td>
                    <td align="right">20</td>
                </tr>
            </table-->
            <a onclick="pop(1)" class="btn btn-block btn-social btn-bitbucket" >
                popupประมาณรอบเก็บเกี่ยวสส
            </a>
            <a onclick="pop(2)" class="btn btn-block btn-social btn-bitbucket" >
                popupประมาณรอบเก็บเกี่ยวจากสูตร
            </a>
            <a onclick="pop(3)" class="btn btn-block btn-social btn-bitbucket" >
                popupคลังสำเร็จรูป
            </a>
            <a onclick="pop(4)" class="btn btn-block btn-social btn-bitbucket" >
                popupสำเร็จรูปพร้อมขาย
            </a>
        </div>
    </div>
</div>
<div id="searchReportStock" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ค้นหาข้อมูล</h4>
      </div>
      <div class="modal-body">
        <form role="form">
            <div class="form-group">
                <label for="ddSeed">ค้นหาจากพืช</label>
                {{ Form::select('ddSeed', $ddSeed,$ddSeed,['class' => 'form-control','id'=>'ddSeed','onchange'=> 'getProduct()']) }}
            </div>
            <div class="form-group">
                <label for="ddProduct">สินค้า</label>
                {{ Form::select('ddProduct', $ddProduct,$ddProduct,['class' => 'form-control','id'=>'ddProduct']) }}
            </div>
            <div class="form-group">
                <label for="ddCompany">คลังบริษัท</label>
                {{ Form::select('ddCompany', $ddCompany,$ddCompany,['class' => 'form-control','id'=>'ddCompany']) }}
            </div>
            <div class="form-group">
                <label for="ddOrderBy">การจัดเรียง</label>
                {{ Form::select('ddOrderBy', $ddOrderBy,$ddOrderBy,['class' => 'form-control','id'=>'ddOrderBy']) }}
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="return false;">ยกเลิก</button>
        <a class="btn btn-primary btn-ok" id="BtnSearchReportStock">ค้นหา</a>
      </div>
    </div>

  </div>
</div>
<div id="pop1" class="modal fade in" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="row">
        <div class="col-md-12">
            <form role="form">
                <div class="row">
                    <div class="col-xs-12">
                        <p><b>ประเภทพืช</b> : แตงกวา</p>
                        <p><b>ชื่อสินค้า</b> : แตงกวา C-665</p>
                        <p>ประมาณวันรอเก็บเกี่ยวทั้งหมด : -446 ถึง 70 วัน</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sort_by" id="sort_by" value="duedate" checked>
                                    วันที่กำหนดรับ
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sort_by" id="sort_by" value="weight">
                                    น้ำหนัก
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sort_type" id="sort_type" value="desc">
                                    เรียงจากมากไปหาน้อย
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sort_type" id="sort_type" value="asc" checked>
                                    เรียงจากน้อยไปหามาก
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="list_type" id="list_type" value="day">
                                    รายวัน
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="list_type" id="list_type" value="month" checked>
                                    รายเดือน
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
  <div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-12">
                <table class="table table-bordered ">
                    <tr>
                        <td><b>บริษัท Abbb</b>:</td>
                        <td align="right">รวมประมาณการผลิตจากสูตร  2,023.57</td>
                    </tr>
                    <tr>
                        <td>เปอร์เซ็นส่วนต่าง 98.06%</td>
                        <td align="right">รวมประมาณการผลิตสส. 10.00</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered ">
                        <thead>
                            <tr bgcolor="#A9E2F3">
                                <th>ลำดับ</th>
                                <th>วันที่กำหนด</th>
                                <th>ระยะเวลา(วัน)</th>
                                <th>ประมาณการผลิตสส.</th>
                                <th>ประมาณการผลิตจากสูตร</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>
                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>
                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>
                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>
                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>
                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>
                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>
                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>

                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>
                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>
                            <tr>
                                <td>ลำดับ</td>
                                <td>วันที่กำหนด</td>
                                <td>ระยะเวลา(วัน)</td>
                                <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <table class="table table-bordered ">
                    <tr>
                        <td><b>บริษัท Aaa</b>:</td>
                        <td align="right">รวมประมาณการผลิตจากสูตร  2,023.57</td>
                    </tr>
                    <tr>
                        <td>เปอร์เซ็นส่วนต่าง 98.06%</td>
                        <td align="right">รวมประมาณการผลิตสส. 10.00</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                        <table class="tree table table-bordered ">
                            <thead>
                                <tr bgcolor="#A9E2F3">
                                    <th>ลำดับ</th>
                                    <th>วันที่กำหนด</th>
                                    <th>ระยะเวลา(วัน)</th>
                                    <th>ประมาณรอเก็บเกี่ยวสส.</th>
                                    <th>ประมาณรอเก็บเกี่ยวจากสูตร</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>
                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>
                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>
                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>
                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>
                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>
                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>
                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>

                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>
                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>
                                <tr>
                                    <td>ลำดับ</td>
                                    <td>วันที่กำหนด</td>
                                    <td>ระยะเวลา(วัน)</td>
                                    <td>ประมาณรอเก็บเกี่ยวสส.</td>
                                    <td>ประมาณรอเก็บเกี่ยวจากสูตร</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="modal-footer">
  </div>
</div>
</div>
</div>
<div id="pop2" class="modal modal-info fade in" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="row">
            <div class="col-md-12">
                <form role="form">
                    <div class="row">
                        <div class="col-xs-12">
                            <p><b>ประเภทพืช</b> : แตงกวา</p>
                            <p><b>ชื่อสินค้า</b> : แตงกวา C-665</p>
                            <p>ประมาณวันรอเก็บเกี่ยวทั้งหมด : -446 ถึง 70 วัน</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12">
                    <table class="tree table table-bordered ">
                        <tr>
                            <td><b>บริษัท Abbb</b>:</td>
                            <td align="right">รวมประมาณการผลิตจากสูตร  2,023.57</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table  table-bordered ">
                            <tr>
                                <td></td>
                                <td></td>
                                <td>จำนวนวันผลิต</td>
                                <td>น้ำหนักเมล็ดทั้งหมดในคลัง</td>
                            </tr>
                            <tr>
                                <td>คลัง 1 รับเมล็ด</td>
                                <td align="right">401.30</td>
                                <td align="right">0</td>
                                <td align="right">1,330.30</td>
                            </tr>
                            <tr>
                                <td>อยู่ระหว่างการตาก(เมล็ดอื่นๆ)</td>
                                <td align="right">0.0</td>
                                <td align="right">0</td>
                                <td align="right">0.0</td>
                            </tr>
                            <tr>
                                <td>ส่งเข้าโรงอบ(ข้าวโพด)</td>
                                <td align="right">0.0</td>
                                <td align="right">0</td>
                                <td align="right">65,689.0</td>
                            </tr>
                            <tr>
                                <td>คลัง 2 ตรวจสอบความสะอาด</td>
                                <td align="right">0.0</td>
                                <td align="right">0</td>
                                <td align="right">0.0</td>
                            </tr>
                            <tr>
                                <td>คลัง 3 คัดเมล็ด</td>
                                <td align="right">32.40</td>
                                <td align="right">0</td>
                                <td align="right">2,372.6</td>
                            </tr>
                            <tr>
                                <td>คลัง 4 รอการเคลือบ/รมยา</td>
                                <td align="right">0</td>
                                <td align="right">0</td>
                                <td align="right">2,372.6</td>
                            </tr>
                            <tr>
                                <td>อยู่ระหว่างเคลือบ/รมยา</td>
                                <td align="right">0</td>
                                <td align="right">0</td>
                                <td align="right">0</td>
                            </tr>
                            <tr>
                                <td>คลัง 5 รอรวม Lot</td>
                                <td align="right">109.60</td>
                                <td align="right">0</td>
                                <td align="right">213.70</td>
                            </tr>
                            <tr>
                                <td>ประมาณจำนวนวันจัดการทั้งหมด</td>
                                <td colspan="3">0</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    <table class="tree table table-bordered ">
                        <tr>
                            <td><b>บริษัท Aaa</b>:</td>
                            <td align="right">รวมประมาณการผลิตจากสูตร  2,023.57</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table  table-bordered ">
                            <tr>
                                <td></td>
                                <td></td>
                                <td>จำนวนวันผลิต</td>
                                <td>น้ำหนักเมล็ดทั้งหมดในคลัง</td>
                            </tr>
                            <tr>
                                <td>คลัง 1 รับเมล็ด</td>
                                <td align="right">401.30</td>
                                <td align="right">0</td>
                                <td align="right">1,330.30</td>
                            </tr>
                            <tr>
                                <td>อยู่ระหว่างการตาก(เมล็ดอื่นๆ)</td>
                                <td align="right">0.0</td>
                                <td align="right">0</td>
                                <td align="right">0.0</td>
                            </tr>
                            <tr>
                                <td>ส่งเข้าโรงอบ(ข้าวโพด)</td>
                                <td align="right">0.0</td>
                                <td align="right">0</td>
                                <td align="right">65,689.0</td>
                            </tr>
                            <tr>
                                <td>คลัง 2 ตรวจสอบความสะอาด</td>
                                <td align="right">0.0</td>
                                <td align="right">0</td>
                                <td align="right">0.0</td>
                            </tr>
                            <tr>
                                <td>คลัง 3 คัดเมล็ด</td>
                                <td align="right">32.40</td>
                                <td align="right">0</td>
                                <td align="right">2,372.6</td>
                            </tr>
                            <tr>
                                <td>คลัง 4 รอการเคลือบ/รมยา</td>
                                <td align="right">0</td>
                                <td align="right">0</td>
                                <td align="right">2,372.6</td>
                            </tr>
                            <tr>
                                <td>อยู่ระหว่างเคลือบ/รมยา</td>
                                <td align="right">0</td>
                                <td align="right">0</td>
                                <td align="right">0</td>
                            </tr>
                            <tr>
                                <td>คลัง 5 รอรวม Lot</td>
                                <td align="right">109.60</td>
                                <td align="right">0</td>
                                <td align="right">213.70</td>
                            </tr>
                            <tr>
                                <td>ประมาณจำนวนวันจัดการทั้งหมด</td>
                                <td colspan="3">0</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>
<div id="pop3" class="modal modal-info fade in" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="row">
            <div class="col-md-4">
                <form role="form">
                    <div class="row">
                        <div class="col-xs-12">
                            <p><b>ประเภทพืช</b> : แตงกวา</p>
                            <p><b>ชื่อสินค้า</b> : แตงกวา C-665</p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
                <form role="form">
                    <div class="row">
                        <div class="col-xs-12">
                            <p>Group/Serial : <input type="text" class="form-control"></p>
                            <p>เกรด : <input type="text" class="form-control"></p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <form role="form">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>ความงอก</td>
                                        <td><input type="number" class="form-control"></td>
                                        <td>ถึง</td>
                                        <td><input type="number" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td>ความชื้น</td>
                                        <td><input type="number" class="form-control"></td>
                                        <td>ถึง</td>
                                        <td><input type="number" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td>ความบริสุทธิ์</td>
                                        <td><input type="number" class="form-control"></td>
                                        <td>ถึง</td>
                                        <td><input type="number" class="form-control"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered ">
                        <thead>
                            <th>ลำดับ</th>
                            <th>เลขที่ Lot</th>
                            <th>เลขที่ Lot อ้างอิง</th>
                            <th>วันที่ Lot</th>
                            <th>ฤดูผลิต</th>
                            <th>น้ำหนัก(Kg)</th>
                            <th>เกรด</th>
                            <th>ความงอก</th>
                            <th>ความชื้น</th>
                            <th>ความบริสุทธิ์</th>
                            <th>หมายเหตุ</th>
                        </thead>
                        <tbody>
                            <td>ลำดับ</td>
                            <td>เลขที่ Lot</td>
                            <td>เลขที่ Lot อ้างอิง</td>
                            <td>วันที่ Lot</td>
                            <td>ฤดูผลิต</td>
                            <td>น้ำหนัก(Kg)</td>
                            <td>เกรด</td>
                            <td>ความงอก</td>
                            <td>ความชื้น</td>
                            <td>ความบริสุทธิ์</td>
                            <td>หมายเหตุ</td>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>
@stop