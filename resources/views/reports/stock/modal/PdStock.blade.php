<link rel="stylesheet" href="{!! asset('css/treeTable.css') !!}">
<div class="modal-dialog modal-lg">
    <div class="nav-tabs-custom">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <ul class="nav nav-tabs">
                <li class="active" id="tab-stock"><a href="#stock" data-toggle="tab">สต๊อกสินค้าวัตถุดิบ</a></li>
                <li id="tab-item"><a href="#item" data-toggle="tab">จำนวนMin/Max สินค้าวัตถุดิบ</a></li>
            </ul>
        </div>
        <div class="modal-body">
            <div class="tab-content">
                <div class="active tab-pane" id="stock">
                    <div class="row">
                        <div class="col-md-4">
                            <form role="form">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p><b>ประเภทพืช</b> : {{ $data['PdName']}}</p>
                                        <p><b>ชื่อสินค้า</b> : {{ $data['PlantName']}}</p>
                                        <p><button type="button" class="btn btn-primary" onclick="PdStock('Tab1','{{ $data['PdName'] }}','{{ $data['PlantName'] }}','{{ $data['Weight'] }}',{{ $data['idPd'] }},{{ $data['idcomp'] }},{{ $data['idcomp'] }})" id="btn-ok">แสดงข้อมูล</button></p>
                                        <p></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-2">
                            <form role="form">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p>Group/Serial : <input type="text" id="LotCodeRef" name="LotCodeRef" class="form-control"></p>
                                        <p>เลขที่Lot : <input type="text" id="LotNum" name="LotNum" class="form-control"></p>
                                        <p>เกรด : <input type="text" id="Grad" name="Grad" class="form-control"></p>
                                        <p><input type="checkbox" id="ChkNoPd" name="ChkNoPd">ไม่แสดงสินค้าที่หมดแล้ว</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form role="form">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td>ความงอก</td>
                                                    <td><input type="text" id="txtGrowS" name="txtGrowS" class="form-control"></td>
                                                    <td>ถึง</td>
                                                    <td><input type="text" id="txtGrowE" name="txtGrowE" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td>ความชื้น</td>
                                                    <td><input type="text" id="txtHumidS" name="txtHumidS" class="form-control"></td>
                                                    <td>ถึง</td>
                                                    <td><input type="text" id="txtHumidE" name="txtHumidE" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td>ความบริสุทธิ์</td>
                                                    <td><input type="text" id="txtPureS" name="txtPureS" class="form-control"></td>
                                                    <td>ถึง</td>
                                                    <td><input type="text" id="txtPureE" name="txtPureE" class="form-control"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="tree-modal table table-bordered">
                                    <thead>
                                        <td align="right" colspan="5"><b>จำนวนรายการ</b> : {{ number_format($data['Count']) }}</td>
                                        <td align="right"><b>น้ำหนักรวม</b> :  {{ number_format($data['Weight'],2) }}</td>
                                    </thead>
                                    <thead>
                                        <th class="text-center">ลำดับ</th>
                                        <th class="text-center">พืช</th>
                                        <th class="text-center">นน.รับเข้า</th>
                                        <th class="text-center">นน.ใช้ไป</th>
                                        <th class="text-center">นน.คงเหลือ</th>
                                        <th class="text-center">วันที่รับเมล็ดเข้า(ล่าสุด)</th>
                                    </thead>
                                    <tbody>
                                        @php 
                                            $i=0;
                                        @endphp
                                        @foreach ($data['items'] as $SeedTypeName=>$rows)
                                            @php 
                                                $i++;
                                            @endphp
                                            <tr data-level="0" >
                                                <td class="text-center">{{ $i }}</td>
                                                <td>{{ $SeedTypeName  }}</td>
                                                <td align="right">{{ number_format($data['sumAmountRec'],2) }}</td>
                                                <td align="right">{{ number_format($data['sumAmountUse'],2)  }}</td>
                                                <td align="right">{{ number_format($data['sumAmount'],2)  }}</td>
                                                <td class="text-center">{{ $data['DateRec'] }}</td>
                                            </tr>
                                            @foreach ($rows as $PdName=>$item)
                                                <tr data-level="1" >
                                                    <td></td>
                                                    <td>{{ $PdName  }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr data-level="2" >
                                                    <td></td>
                                                    <td colspan="5"style="overflow-x:scroll;">
                                                        <div style="width:600px">
                                                            <table class="table table-bordered" style="width:1800px">
                                                                <thead>
                                                                    <th class="text-center">ลำดับ</th>
                                                                    <th class="text-center">Group/Serial</th>
                                                                    <th class="text-center" width="120px">วันทีรับเมล็ดเข้า</th>
                                                                    <th class="text-center" width="100px">เลขที่ใบแพ็ค</th>
                                                                    <th class="text-center">เลขที่Lot</th>
                                                                    <th class="text-center">เกรด</th>
                                                                    <th class="text-center">นน.รับเข้า</th>
                                                                    <th class="text-center">นน.ใช้ไป</th>
                                                                    <th class="text-center">นน.คงเหลือ</th>
                                                                    <th class="text-center">ความงอก</th>
                                                                    <th class="text-center">ความชื้น</th>
                                                                    <th class="text-center">ความบริสุทธิ์</th>
                                                                    <th class="text-center">ความงอกรับมา</th>
                                                                    <th class="text-center">ความชื้นรับมา</th>
                                                                    <th class="text-center">ความบริสุทธิ์รับมา</th>
                                                                    <th class="text-center">AA</th>
                                                                    <th class="text-center">TZ</th>
                                                                    <th class="text-center">Paper</th>
                                                                    <th class="text-center">Sand</th>
                                                                    <th class="text-center">หมายเหตุ</th>
                                                                    <th class="text-center">ชื่อคลัง</th>                                                        
                                                                </thead>
                                                                <tbody>
                                                                @foreach ($item as $index=>$row)
                                                                    <tr>
                                                                        <td class="text-center">{{ $index+1}}</td>
                                                                        <td style="padding-left: 0px;">{{ $row['LotCodeRef'] }}</td>
                                                                        <td class="text-center">{{ $row['DateRec'] }}</td>
                                                                        <td class="text-center">{{ $row['DocPackCode'] }}</td>
                                                                        <td class="text-center">{{ $row['LotNum'] }}</td>
                                                                        <td class="text-center">{{ $row['Grad'] }}</td>
                                                                        <td align="right">{{ number_format($row['AmountRec'],2) }}</td>
                                                                        <td align="right">{{ number_format($row['AmountUse'],2) }}</td>
                                                                        <td align="right">{{ number_format($row['Amount'],2) }}</td>
                                                                        <td class="text-center">{{ $row['Grow'] }}</td>
                                                                        <td class="text-center">{{ $row['Humid'] }}</td>
                                                                        <td class="text-center">{{ $row['Pure'] }}</td>
                                                                        <td class="text-center">{{ $row['GrowRec'] }}</td>
                                                                        <td class="text-center">{{ $row['HumidRec'] }}</td>
                                                                        <td class="text-center">{{ $row['PureRec'] }}</td>
                                                                        <td class="text-center">{{ $row['GrowAA'] }}</td>
                                                                        <td class="text-center">{{ $row['GrowTZ'] }}</td>
                                                                        <td class="text-center">{{ $row['GrowPaper'] }}</td>
                                                                        <td class="text-center">{{ $row['GrowSand'] }}</td>
                                                                        <td class="text-center">{{ $row['Note'] }}</td>
                                                                        <td class="text-center"></td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ddSeed">ค้นหาจากพืช</label>
                                {{ Form::select('ddSeedTab', $ddSeed,$idSeed,['class' => 'form-control','id'=>'ddSeedTab','onchange'=> 'getProductTab("-")']) }}
                            </div>
                            <div class="form-group">
                                <label for="ddProduct">สินค้า</label>
                                {{ Form::select('ddProductTab', $ddProduct,$idProduct,['class' => 'form-control','id'=>'ddProductTab']) }}
                            </div>
                            <div class="form-group">
                                <p><button type="button" class="btn btn-primary" onclick="PdStock('Tab2','{{ $data['PdName'] }}','{{ $data['PlantName'] }}','{{ $data['Weight'] }}',{{ $data['idPd'] }},{{ $data['idcomp'] }},{{ $data['idcomp'] }})" id="btn-ok">แสดงข้อมูล</button></p>
                            </div>
                        </div>
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-6 CHK">
                                @php
                                    $CHKMin="";
                                    $CHKMax="";
                                    $CHKFixMinMax="checked";
                                    $CHKAll="";
                                    if($ChkTab2=='CHKMin')
                                    {
                                        $CHKMin='checked';
                                        $CHKFixMinMax="";
                                    }
                                    elseif($ChkTab2=='CHKMax')
                                    {
                                        $CHKMax='checked';
                                        $CHKFixMinMax="";
                                    }
                                    elseif($ChkTab2=='CHKFixMinMax')
                                    {
                                        $CHKFixMinMax='checked';
                                    }
                                    elseif($ChkTab2=='CHKAll')
                                    {
                                        $CHKAll='checked';
                                        $CHKFixMinMax="";
                                    }
                                    else
                                    {
                                        $CHKFixMinMax="checked";
                                    }
                                @endphp
                            <div class="form-group">
                                <input type="radio" id="CHK"  name="CHK" value="CHKMin" {{ $CHKMin }}> 
                                <label for="">จำนวนคงเหลือต่ำกว่าค่าต่ำสุด</label>
                            </div>
                            <div class="form-group">
                                <input type="radio" id="CHK" name="CHK" value="CHKMax" {{ $CHKMax }}> 
                                <label for="">จำนวนคงเหลือสูงกว่าค่าสูงสุด</label>
                            </div>
                            <div class="form-group">
                                <input type="radio" id="CHK"  name="CHK" value="CHKFixMinMax" {{ $CHKFixMinMax }}> 
                                <label for="">จำนวนคงเหลือเกินค่าสูงสุด / ต่ำกว่าค่าต่ำสุด</label>
                            </div>
                            <div class="form-group">
                                <input type="radio" id="CHK"  name="CHK" value="CHKAll" {{ $CHKAll }}> 
                                <label for="">แสดงทุกรายการ</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="tree-modal table table-bordered">
                                <thead>
                                    <th colspan="6"align="right">รายการทั้งหมด {{ count($tab) }} รายการ</th>
                                </thead>
                                <thead>
                                    <th class="text-center">ลำดับ</th>
                                    <th class="text-center">ชื่อสินค้า</th>
                                    <th class="text-center">จำนวนคงเหลือ</th>
                                    <th class="text-center">ค่าสุงสุด</th>
                                    <th class="text-center">ค่าต่ำสุด</th>
                                    <th class="text-center">สถานะ</th>
                                </thead>
                                <tbody>
                                @foreach ($tab as $index=>$item)
                                    <tr>
                                        <td class="text-center">{{  $index+1 }}</td>
                                        <td>{{ $item['PdName'] }}</td>
                                        <td align="right">{{ number_format($item['Amount'],2) }}</td>
                                        <td align="right">{{ number_format($item['MaxOld'],2) }}</td>
                                        <td align="right">{{ number_format($item['MinOld'],2) }}</td>
                                        <td>{{ $item['Note'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
        </div>
    </diV>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $("table.tree-modal").treeTable({
        ignoreClickOn: "input, a, img"
      });
    });
</script>