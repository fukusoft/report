@php
    $modal_size='modal-lg';
    $col_size='col-md-6';
    if(count($data['item'])==1)
    {
        $modal_size='';
        $col_size='col-md-12';
    }
@endphp
<div class="modal-dialog {{ $modal_size }}">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="row">
            <div class="col-md-12">
                <form role="form">
                    <div class="row">
                        <div class="col-xs-12">
                            <p><b>ประเภทพืช</b> : {{ $data['PlantName']}}</p>
                            <p><b>ชื่อสินค้า</b> : {{ $data['PdName']}}</span></p>
                            <p>ประมาณวันรอเก็บเกี่ยวทั้งหมด :  {{ $data['EsDate']}} วัน</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
      <div class="modal-body">
        <div class="row">
            @foreach ($data['item'] as $index=>$item)
                <div class="{{ $col_size }}">
                    <div class="col-md-12">
                        <table class="tree table table-bordered ">
                            <tr>
                                <td><b>บริษัท {{ $item['Company'] }}</b>:</td>
                                <td align="right">รวมประมาณการผลิตจากสูตร  {{ number_format($item['Sum'],2) }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table  table-bordered ">
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>จำนวนวันผลิต</td>
                                    <td>น้ำหนักเมล็ดทั้งหมดในคลัง</td>
                                </tr>
                                <tr>
                                    <td>คลัง 1 รับเมล็ด</td>
                                    <td align="right">{{ number_format($item['S1NOW'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col2_S1'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col3_S1NOW'],2) }}</td>
                                </tr>
                                <tr>
                                    <td>อยู่ระหว่างการตาก(เมล็ดอื่นๆ)</td>
                                    <td align="right">{{ number_format($item['WIP1'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col2_PcDry'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col3_WIP1'],2) }}</td>
                                </tr>
                                <tr>
                                    <td>ส่งเข้าโรงอบ(ข้าวโพด)</td>
                                    <td align="right">{{ number_format($item['Bake'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col2_PcBake'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col3_Bake'],2) }}</td>
                                </tr>
                                <tr>
                                    <td>คลัง 2 ตรวจสอบความสะอาด</td>
                                    <td align="right">{{ number_format($item['S2'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col2_S2'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col3_S2'],2) }}</td>
                                </tr>
                                <tr>
                                    <td>คลัง 3 คัดเมล็ด</td>
                                    <td align="right">{{ number_format($item['S3'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col2_S3'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col3_S3'],2) }}</td>
                                </tr>
                                <tr>
                                    <td>คลัง 4 รอการเคลือบ/รมยา</td>
                                    <td align="right">{{ number_format($item['S4'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col2_S4'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col3_S4'],2) }}</td>
                                </tr>
                                <tr>
                                    <td>อยู่ระหว่างเคลือบ/รมยา</td>
                                    <td align="right">{{ number_format($item['WIP4'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col2_S4'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col3_WIP4'],2) }}</td>
                                </tr>
                                <tr>
                                    <td>คลัง 5 รอรวม Lot</td>
                                    <td align="right">{{ number_format($item['S5'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col2_S5'],2) }}</td>
                                    <td align="right">{{ number_format($item['Col3_S5'],2) }}</td>
                                </tr>
                                <tr>
                                    <td>ประมาณจำนวนวันจัดการทั้งหมด</td>
                                    <td colspan="3">{{ $item['SumDay'] }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
      </div>
    </div>
</div>
