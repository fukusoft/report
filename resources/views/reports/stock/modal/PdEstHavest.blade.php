@php
    $modal_size='modal-lg';
    $col_size='col-md-6';
    if(count($data['item'])==1)
    {
        $modal_size='';
        $col_size='col-md-12';
    }
@endphp
<div class="modal-dialog {{ $modal_size }}">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="row">
            <div class="col-md-12">
                <form role="form">
                    <div class="row">
                        <div class="col-xs-12">
                            <p><b>ประเภทพืช</b> : {{ $data['PlantName']}}</p>
                            <p><b>ชื่อสินค้า</b> : {{ $data['PdName']}}</p>
                            <p>ประมาณวันรอเก็บเกี่ยวทั้งหมด : {{ $data['EsDate']}} วัน</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                @php
                                    $duedate="";
                                    $weight="";
                                    $desc="";
                                    $day="";
                                    $asc="";
                                    $month="";
                                    if($data['sort_by']=='duedate')
                                    {
                                        $duedate='checked';
                                    }
                                    else
                                    {
                                        $weight='checked';
                                    }
                                    if($data['sort_type']=='asc')
                                    {
                                        $asc='checked';
                                    }
                                    else
                                    {
                                        $desc='checked';
                                    }
                                    if($data['list_type']=='day')
                                    {
                                        $day='checked';
                                    }
                                    else
                                    {
                                        $month='checked';
                                    }
                                @endphp
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="PdEstHavest_sort_by" id="PdEstHavest_sort_by" value="duedate" {{ $duedate }}>
                                        วันที่กำหนดรับ
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="PdEstHavest_sort_by" id="PdEstHavest_sort_by" value="weight"  {{ $weight }}>
                                        น้ำหนัก
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="PdEstHavest_sort_type" id="PdEstHavest_sort_type" value="desc" {{ $desc }}>
                                        เรียงจากมากไปหาน้อย
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="PdEstHavest_sort_type" id="PdEstHavest_sort_type" value="asc" {{ $asc }}>
                                        เรียงจากน้อยไปหามาก
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="PdEstHavest_list_type" id="PdEstHavest_list_type" value="day" {{ $day }}>
                                        รายวัน
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="PdEstHavest_list_type" id="PdEstHavest_list_type" value="month" {{ $month }}>
                                        รายเดือน
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="button" class="btn btn-primary" onclick="PdEstHavest('{{ $data['PdName'] }}','{{ $data['PlantName'] }}','{{ $data['EsDate'] }}',{{ $data['idPd'] }},{{ $data['idComp'] }},{{ $data['idComp'] }})" id="btn-ok">แสดงข้อมูล</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
      <div class="modal-body">
        <div class="row">
            @foreach ($data['item'] as $index=>$item)
            <div class="{{ $col_size }}">
                <div class="col-md-12">
                    <table class="table table-bordered ">
                        <tr>
                            <td><b>บริษัท {{ $item['Company'] }}</b>:</td>
                            <td align="right">รวมประมาณการผลิตจากสูตร  {{number_format($item['sum'],2)}}</td>
                        </tr>
                        <tr>
                            <td>เปอร์เซ็นส่วนต่าง {{$item['txt_dif'] }}</td>
                            <td align="right">รวมประมาณการผลิตสส. {{$item['sumPs'] }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive" style="max-height:200px !important;">
                        <table class="table table-bordered" >
                            <thead>
                                <tr bgcolor="#A9E2F3">
                                    <th class="text-center">ลำดับ</th>
                                    <th class="text-center">วันที่กำหนด</th>
                                    <th class="text-center">ระยะเวลา(วัน)</th>
                                    <th class="text-center">ประมาณการผลิตสส.</th>
                                    <th class="text-center">ประมาณการผลิตจากสูตร</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($item['items'] as $index=>$row)
                                @if($item['status'])
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td align="center">{{ $row['DateReceive_'] }}</td>
                                    <td align="center">{{ $row['col_date'] }}</td>
                                    <td align="right">{{ number_format($row['PsEstimate'],2) }}</td>
                                    <td align="right">{{ number_format($row['EstimateW'],2) }}</td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
</div>
<script>
