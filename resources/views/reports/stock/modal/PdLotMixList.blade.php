<div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="row">
            <div class="col-md-4">
                <form role="form">
                    <div class="row">
                        <div class="col-xs-12">
                            <p><b>ประเภทพืช</b> : {{ $data['PdName']}}</p>
                            <p><b>ชื่อสินค้า</b> : {{ $data['PlantName']}}</p>
                            <p></p>
                            <p></p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
                <form role="form">
                    <div class="row">
                        <div class="col-xs-12">
                            <p>Group/Serial : <input type="text" id="LotCode" name="LotCode" class="form-control"></p>
                            <p>เกรด : <input type="text" id="Grad" name="Grad" class="form-control"></p>
                            <p><button type="button" class="btn btn-primary" onclick="PdLotMixList('{{ $data['PdName'] }}','{{ $data['PlantName'] }}','{{ $data['Weight'] }}',{{ $data['idPd'] }},{{ $data['idcomp'] }},{{ $data['idcomp'] }})" id="btn-ok">แสดงข้อมูล</button></p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <form role="form">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>ความงอก</td>
                                        <td><input type="text" id="txtGrowS" name="txtGrowS" class="form-control"></td>
                                        <td>ถึง</td>
                                        <td><input type="text" id="txtGrowE" name="txtGrowE" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td>ความชื้น</td>
                                        <td><input type="text" id="txtHumidS" name="txtHumidS" class="form-control"></td>
                                        <td>ถึง</td>
                                        <td><input type="text" id="txtHumidE" name="txtHumidE" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td>ความบริสุทธิ์</td>
                                        <td><input type="text" id="txtPureS" name="txtPureS" class="form-control"></td>
                                        <td>ถึง</td>
                                        <td><input type="text" id="txtPureE" name="txtPureE" class="form-control"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered ">
                        <thead>
                            <td align="right" colspan="10"><b>จำนวนรายการ</b> : {{ number_format($data['Count']) }}</td>
                            <td align="right"><b>น้ำหนักรวม</b> :  {{ number_format($data['Weight'],2) }}</td>
                        </thead>
                        <thead>
                            <th class="text-center">ลำดับ</th>
                            <th class="text-center">เลขที่ Lot</th>
                            <th class="text-center">เลขที่ Lot อ้างอิง</th>
                            <th class="text-center">วันที่ Lot</th>
                            <th class="text-center">ฤดูผลิต</th>
                            <th class="text-center">น้ำหนัก(Kg)</th>
                            <th class="text-center">เกรด</th>
                            <th class="text-center">ความงอก</th>
                            <th class="text-center">ความชื้น</th>
                            <th class="text-center">ความบริสุทธิ์</th>
                            <th class="text-center">หมายเหตุ</th>
                        </thead>
                        <tbody>
                            @foreach ($data['items'] as $index=>$item)
                                <tr>
                                    <td class="text-center">{{ $index+1 }}</td>
                                    <td class="text-center">{{ $item['LotNum'] }}</td>
                                    <td class="text-center">{{ $item['LotCode'] }}</td>
                                    <td class="text-center">{{ $item['DateLot'] }}</td>
                                    <td class="text-center">{{ $item['SeasonName'] }}</td>
                                    <td align="right">{{ $item['WUpdate'] }}</td>
                                    <td class="text-center">{{ $item['Grad'] }}</td>
                                    <td align="right">{{ $item['Grow'] }}</td>
                                    <td align="right">{{ $item['Humid'] }}</td>
                                    <td align="right">{{ $item['Pure'] }}</td>
                                    <td>{{ $item['ColNote'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
</div>
