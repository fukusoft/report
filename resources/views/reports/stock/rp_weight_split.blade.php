@extends('reports.stock.rp_stock')
@section('sub_content')
<div class="table-responsive">
    <table class="tree table table-bordered">           
        <thead>
            <tr bgcolor="#A9E2F3">
                <th style="width: 10px">ลำดับ</th>
                <th width="25%">สินค้า</th>
                <th width="15%">พืช</th>
                <th>ประมาณรอเก็บเกี่ยวสส.</th>
                <th>ประมาณรอเก็บเกี่ยวจากสูตร</th>
                <th>ประมาณวันรอเก็บเกี่ยว</th>
                <th>เมล็ดระหว่างกระบวนการ</th>
                <th>ประมาณวันเสร็จกระบวนการในคลัง</th>
                <th>คลังสำเร็จรูป</th>
                <th>ประมาณน้ำหนักทั้งหมด</th>
            </tr>
        </thead>
        <tbody>
        @php
            $i=0;
            $ckProductName="";
            $PsEstimate=0;
            $EstimateW=0;
            $BalancePc=0;
            $AvStock=0;
            $S6=0;
            $SumWeight=0;
            $PdName="";
            $PlantName="";
        @endphp
        @foreach ($Results as $ProductName=>$data)
            @php
                $i++;
                $Flotech_S6 = isset($data['Flotech']['S6']) ? $data['Flotech']['S6'] : 0;
                $Flotech_BalancePc = isset($data['Flotech']['BalancePc']) ? $data['Flotech']['BalancePc'] : 0;
                $Flotech_PsEstimate = isset($data['Flotech']['PsEstimate']) ? $data['Flotech']['PsEstimate'] : 0;
                $GreenSeeds_S6 = isset($data['GreenSeeds']['S6']) ? $data['GreenSeeds']['S6'] : 0;
                $GreenSeeds_BalancePc = isset($data['GreenSeeds']['BalancePc']) ? $data['GreenSeeds']['BalancePc'] : 0;
                $GreenSeeds_PsEstimate = isset($data['GreenSeeds']['PsEstimate']) ? $data['GreenSeeds']['PsEstimate'] : 0;
                $sumS6=$Flotech_S6+$GreenSeeds_S6;
                $sumBalancePc=$Flotech_BalancePc+$GreenSeeds_BalancePc;
                $sumPsEstimate=$Flotech_PsEstimate+$GreenSeeds_PsEstimate;
                if (array_key_exists("Flotech",$data))
                {
                    $AvStock=$data['Flotech']['AvStock'];
                    $SeedTypeName=$data['Flotech']['SeedTypeName'];
                }
                elseif(array_key_exists("GreenSeeds",$data))
                {
                    $AvStock=$data['GreenSeeds']['AvStock'];
                    $SeedTypeName=$data['GreenSeeds']['SeedTypeName'];
                }
                else
                {
                    $AvStock=$data['AVS']['AvStock'];
                    $SeedTypeName=$data['AVS']['SeedTypeName'];
                }
                $weight=$AvStock+$sumS6+$sumBalancePc+$sumPsEstimate;
            @endphp
            @if($ckProductName=="")
                @php
                    $ckProductName=$ProductName
                @endphp
            @elseif($ckProductName!=$ProductName)
                @php
                    $ckProductName=$ProductName
                @endphp
            @endif
            <tr data-level="0" bgcolor="#E6E6FA">
                <td>{{ $i }}</td>
                <td>{{ $ProductName }}</td>
                <td colspan="7">{{ $SeedTypeName }}</td>
                <td align="right">{{ number_format($weight,2) }}</td>
            </tr>
            @foreach ($data as $Company=>$item)
                @php
                    $PsEstimate=$item['PsEstimate'];
                    $EstimateW=$item['EstimateW'];
                    $AvStock=$item['AvStock'];
                    $BalancePc=$item['BalancePc'];
                    $S6=$item['S6'];
                    $PdName=$item['PdName'];
                    $PlantName=$item['PlantName'];
                    $SumWeight=$BalancePc+$S6+$PsEstimate;
                    $EsDate=$item['EsDate'];
                    $EPDate=$item['EPDate'];
                    $idPd=$item['idPd'];
                    $idCompb=$item['idCompb'];
                @endphp
                <tr data-level="1">
                    <td></td>
                    <td>{{ $Company }}</td>
                    <td></td>
                    <td align="right">
                    @if($PsEstimate!=0)
                    <a onclick="PdEstPC('{{ $ProductName }}','{{ $SeedTypeName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},{{$idCompb}})">{{ number_format($PsEstimate,2) }}</a>
                    @else
                        {{ number_format($PsEstimate,2) }}
                    @endif
                    </td>
                    <td align="right">
                    @if($EstimateW!=0)
                    <a onclick="PdEstPC('{{ $ProductName }}','{{ $SeedTypeName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},{{$idCompb}})">{{ number_format($EstimateW,2) }}</a>
                    @else
                        {{ number_format($EstimateW,2) }}
                    @endif
                    </td>
                    <td align="center">{{ $EsDate }}</td>
                    <td align="right">
                    @if($BalancePc!=0)
                        <a onclick="PdEstHavest('{{ $ProductName }}','{{ $SeedTypeName }}','{{ $EsDate }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($BalancePc,2) }}</a>
                    @else
                        {{ number_format($BalancePc,2) }}
                    @endif
                    </td>
                    <td align="center">{{ $EPDate }}</td>
                    <td align="right">
                    @if($S6!=0)
                        <a onclick="PdLotMixList('{{ $ProductName }}','{{ $SeedTypeName }}','{{ $S6 }}',{{ $idPd }},{{ $ReportType }},{{ $idCompb }})">{{ number_format($S6,2) }}</a>
                    @else
                        {{ number_format($S6,2) }}
                    @endif
                    </td>
                    <td align="right">{{ number_format($SumWeight,2) }}</td>
                </tr>
            @endforeach
            <tr data-level="1">
                <td></td>
                <td>AVS</td>
                <td></td>
                <td align="right"></td>
                <td align="right"></td>
                <td align="right"></td>
                <td align="right"></td>
                <td align="right"></td>
                <td align="right"></td>
                <td align="right">{{ number_format($AvStock,2) }}</td>
            </tr>
        @endforeach
        </tbody> 
    </table>
</div>
@stop