@extends('layouts.main')
@section('content')
<div class="col-md-12">
    <div class="box">
        <div class="box-header">
        <h3 class="box-title"></h3>
        <div class="box-tools">
            <form action="" method="get">
            <label for="">บริษัท</label>
            <select name="" id="">
                <option value="">บริษัททั้งหมด</option>
                <option value="">บริษัท A</option>
                <option value="">บริษัท B</option>
                <option value="">บริษัท C</option>
            </select>
            <label for="">ประเภทเมล็ด</label>
            <select name="" id="">
                <option value="">ประเภทเมล็ดทั้งหมด</option>
                <option value="">แตงโม1</option>
                <option value="">แตงโม2</option>
            </select>
            <label for="">สายพันธุ์</label>
            <select name="" id="">
                <option value="">สายพันธุ์ทั้งหมด</option>
                <option value="">สายพันธุ์A</option>
                <option value="">สายพันธุ์B</option>
            </select>
            <input type="text" placeholder="Search...">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat">ค้นหา
            </button>
            </form>
        </div>
    </div>
<!-- /.box-header -->
    <div class="box-body no-padding">
        <table class="tree table table-bordered table-striped">
            <tr>
                <th style="width: 10px">ลำดับ</th>
                <th width="20%">พืช</th>
                <th>ประมาณรอเก็บสส</th>
                <th>ประมาณรอเก็บจากสูตร</th>
                <th>ประมาณรอเก็บเกี่ยว</th>
                <th>เมล็ดระหว่างกระบวนการ</th>
                <th>ประมาณวันเสร็จกระบวนการในคลัง</th>
                <th>คลังสำเร็จรูป</th>
                <th>ประมาณน้ำหนักทั้งหมด</th>
            </tr>
            <tr data-level="0">
                <td>1</td>
                <td>ข้าวโพดหวาน</td>
                <td align="right">300</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
            </tr>
            <tr data-level="1">
                <td></td>
                <td>ข้าวโพดหวาน Sun06</td>
                <td align="right">200</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
            </tr>
            <tr data-level="2">
                <td></td>
                <td>-Flo</td>
                <td align="right">50</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
            </tr>
            <tr data-level="2">
                <td></td>
                <td>-Green</td>
                <td align="right">100</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
            </tr>
            <tr data-level="2">
                <td></td>
                <td>-AVS</td>
                <td align="right">50</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
            </tr>
            <tr data-level="1">
                <td></td>
                <td>ข้าวโพดหวาน Sun09</td>
                <td align="right">100</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
            </tr>
            <tr data-level="2">
                <td></td>
                <td>-Flo</td>
                <td align="right">20</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
            </tr>
            <tr data-level="2">
                <td></td>
                <td>-Green</td>
                <td align="right">50</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
            </tr>
            <tr data-level="2">
                <td></td>
                <td>-AVS</td>
                <td align="right">30</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
                <td align="right">0</td>
            </tr>
        </table>
        <hr>
        <table class="table">
        
            <tr>
                <th style="width: 10px">#</th>
                <th>บริษัท</th>
                <th>ประเภทเมล็ด</th>
                <th>สายพันธุ์</th>
                <th>จำนวน</th>
            </tr>
            @foreach ($list as $index=>$user)
            <tr>
                <td>1</td>
                <td>A</td>
                <td>
                {{ $user->BankName  }}
                </td>
                <td>สายพันธุ์A</td>
                <td>20/50</td>
            </tr>
            @endforeach
            <!--tr>
                <td>2.</td>
                <td>A</td>
                <td>
                    แตงโม2
                </td>
                <td>สายพันธุ์B</td>
                <td>50/50</td>
            </tr>
            <tr>
                <td>3.</td>
                <td>B</td>
                <td>
                    แตงโม1
                </td>
                <td>สายพันธุ์B</td>
                <td>50/50</td>
            </tr>    
            <tr>
                <td>4.</td>
                <td>C</td>
                <td>
                    แตงโม1
                </td>
                <td>สายพันธุ์B</td>
                <td>0/50</td>
            </tr-->   
        </table>
        <div class="box-tools">
        {{ $list->links() }}
        </div>
    </div>

<!-- /.box-body -->
</div>
@stop