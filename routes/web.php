<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ReportController@index');
Route::get('/demo', 'ReportController@demo');
Route::get('/dd/product/{id}/ddtype/{type}', 'DropdownController@list_seed_product');
Route::get('/PdEstPC/ProductName/{PdName}/PlantName/{PlantName}/ProductId/{idPd}/Company/{idcomp}/EsDate/{EsDate}', 'ReportController@PdEstPC');
Route::get('/PdEstHavest/ProductName/{PdName}/PlantName/{PlantName}/ProductId/{idPd}/Company/{idcomp}/EsDate/{EsDate}/SortBy/{sort_by}/SortType/{sort_type}/ListType/{list_type}', 'ReportController@PdEstHavest');
Route::get('/PdLotMixList', 'ReportController@PdLotMixList');
Route::get('/PdStock', 'ReportController@PdStock');
Route::get('/stock', 'ReportController@index');
Route::post('/stock', 'ReportController@report_stock');
