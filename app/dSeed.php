<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dSeed extends Model
{
    public $timestamps = false;
    protected $table = 'dSeed'; 
    protected $primaryKey = 'idSeed'; 
    protected $fillable = ['idSeed','SeedName','idSeedKind','stDel'];
}
