<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;

class ReportController extends DropdownController
{
/**************report_stock***************/
    public function index()
    {
        $ddSeed = $this->list_seed();
        $ddProduct = $this->list_seed_product(Config::get('constants.SeedId.Default'),null);
        $ddCompany = $this->list_company();
        $ddOrderBy = $this->list_orderBy();
        $Results = array();
        return view('reports.stock.rp_stock')->with([
            'ddSeed' => $ddSeed,
            'ddProduct' => $ddProduct,
            'ddCompany' => $ddCompany,
            'ddOrderBy' => $ddOrderBy,
            'Results' =>  $Results,
            'OrderType' =>'',
            'ReportType' => 0,
            'ALLSUM' => Config::get('constants.Company.ALLSUM.Id'),
            'ALLSPLIT' => Config::get('constants.Company.ALLSPLIT.Id'),
        ]);
    }
    public function report_stock(Request $request)
    {
        $input = $request->all();
        $idSeed = $input['ddSeed'];
        $idProduct = $input['ddProduct'];
        $idCompany = $input['ddCompany'];
        $order_by = $input['ddOrderBy'];
        $sql_seed = $this->report_stock_seed($idCompany,$idSeed,$idProduct);
        $sql_company = $this->report_stock_company($idCompany);
        $sql_order = $this->report_stock_orderBy($idCompany,$order_by);
        $sql = $this->report_stock_main($idCompany,$sql_company,$sql_seed);
        $fullsql = $sql.$sql_order;
        $results=DB::select($fullsql);
        $newResults = $this->report_stock_group_manage_results($results,$order_by,$idCompany);
        return $this->view_condition($newResults,$order_by,$idCompany);
        
    }
    public function view_condition($results,$order_by,$report_type)
    {
        $ddSeed = $this->list_seed();
        $ddProduct = $this->list_seed_product(Config::get('constants.SeedId.Default'),null);
        $ddCompany = $this->list_company();
        $ddOrderBy = $this->list_orderBy();
        $ALLSUM = Config::get('constants.Company.ALLSUM.Id');
        $ALLSPLIT = Config::get('constants.Company.ALLSPLIT.Id');
        if(count($results)>0)
        {
            if($order_by=='weight')
            {
                if($report_type!=$ALLSUM)
                {
                    if($report_type>0)
                    {
                        return view('reports.stock.rp_weight_company')->with([
                            'ddSeed' => $ddSeed,
                            'ddProduct' => $ddProduct,
                            'ddCompany' => $ddCompany,
                            'ddOrderBy' => $ddOrderBy,
                            'Results' => $results ,
                            'OrderType' => $order_by,
                            'ReportType' => $report_type,
                            'ALLSUM' => $ALLSUM,
                            'ALLSPLIT' => $ALLSPLIT,
                        ]);;
                    }
                    else
                    {
                        return view('reports.stock.rp_weight_split')->with([
                            'ddSeed' => $ddSeed,
                            'ddProduct' => $ddProduct,
                            'ddCompany' => $ddCompany,
                            'ddOrderBy' => $ddOrderBy,
                            'Results' => $results ,
                            'OrderType' => $order_by,
                            'ReportType' => $report_type,
                            'ALLSUM' => $ALLSUM,
                            'ALLSPLIT' => $ALLSPLIT,
                        ]);;
                    }
                }
                else
                {
                    return view('reports.stock.rp_weight_allsum')->with([
                        'ddSeed' => $ddSeed,
                        'ddProduct' => $ddProduct,
                        'ddCompany' => $ddCompany,
                        'ddOrderBy' => $ddOrderBy,
                        'Results' => $results ,
                        'OrderType' => $order_by,
                        'ReportType' => $report_type,
                        'ALLSUM' => $ALLSUM,
                        'ALLSPLIT' => $ALLSPLIT,
                    ]);;
                }
            }
            else
            {
                if($report_type!=$ALLSUM)
                {
                    if($report_type>0)
                    {
                        return view('reports.stock.rp_plant_company')->with([
                            'ddSeed' => $ddSeed,
                            'ddProduct' => $ddProduct,
                            'ddCompany' => $ddCompany,
                            'ddOrderBy' => $ddOrderBy,
                            'Results' => $results ,
                            'OrderType' => $order_by,
                            'ReportType' => $report_type,
                            'ALLSUM' => $ALLSUM,
                            'ALLSPLIT' => $ALLSPLIT,
                        ]);;
                    }
                    else
                    {
                        return view('reports.stock.rp_plant_split')->with([
                            'ddSeed' => $ddSeed,
                            'ddProduct' => $ddProduct,
                            'ddCompany' => $ddCompany,
                            'ddOrderBy' => $ddOrderBy,
                            'Results' => $results ,
                            'OrderType' => $order_by,
                            'ReportType' => $report_type,
                            'ALLSUM' => $ALLSUM,
                            'ALLSPLIT' => $ALLSPLIT,
                        ]);;
                    }
                }
                else
                {
                    return view('reports.stock.rp_plant_allsum')->with([
                        'ddSeed' => $ddSeed,
                        'ddProduct' => $ddProduct,
                        'ddCompany' => $ddCompany,
                        'ddOrderBy' => $ddOrderBy,
                        'Results' => $results ,
                        'OrderType' => $order_by,
                        'ReportType' => $report_type,
                        'ALLSUM' => $ALLSUM,
                        'ALLSPLIT' => $ALLSPLIT,
                    ]);
                }
            }
        }
        else
        {
            
            return view('reports.stock.rp_stock')->with([
                'ddSeed' => $ddSeed,
                'ddProduct' => $ddProduct,
                'ddCompany' => $ddCompany,
                'ddOrderBy' => $ddOrderBy,
                'Results' =>  $results,
                'OrderType' =>'',
                'ReportType' => 0,
                'ALLSUM' => $ALLSUM,
                'ALLSPLIT' => $ALLSPLIT,
            ]);
        }
    }
    public function report_stock_group_manage_results($results,$order_by,$idCompany)
    {
        if($order_by=='weight')
        {
            $newResults = $this->report_stock_group_data_weight($results,$idCompany);
        }
        else
        {
            $newResults = $this->report_stock_group_data($results,$idCompany);
        }
        return $newResults;
    }
    public function report_stock_group_data_weight($results,$idCompany)
    {
        $newResults = array();
        $resultsDateEP_1=$this->getDateEP_idCompb1();
        $resultsDateEP_2=$this->getDateEP_idCompb2();
        foreach($results as $key => $item)
        {
            $sum=0;
            if($idCompany==Config::get('constants.Company.AVS.Id'))
            {
                $sum=$results[$key]->AvStock;
            }
            $sum+=$results[$key]->PsEstimate+$results[$key]->EstimateW+$results[$key]->BalancePc+$results[$key]->S6;
            if($results[$key]->PdName!='-'&&$results[$key]->PlantName!=null&&$sum!=0)
            {
                $MaxDateReceive1=$results[$key]->maxDateReceive_1;
                $MinDateReceive1=$results[$key]->minDateReceive_1;
                $MaxDateReceive2=$results[$key]->maxDateReceive_2;
                $MinDateReceive2=$results[$key]->minDateReceive_2;
                $EsDate = $this->get_dDataAllsum($MaxDateReceive1,$MinDateReceive1,$MaxDateReceive2,$MinDateReceive2);
                $EpDate = $this->getDateEP($results[$key]->idSeedType,$resultsDateEP_1,$resultsDateEP_2);
                if(trim($item->PdName)=="แคนตาลูป เนื้อส้ม D-25"&&trim($item->SeedTypeName)=="แคนตาลูป เนื้อแดง")
                {

                }
                else
                {
                    $new_item = array(
                        'PdName' =>  $item->PdName,
                        'SeedTypeName' =>  $item->SeedTypeName,
                        'idSeed' =>  $item->idSeed,
                        'PlantName' =>  $item->PlantName,
                        'AvStock' =>  (float)$item->AvStock,
                        'PsEstimate' =>  (float)$item->PsEstimate,
                        'EstimateW' =>  (float)$item->EstimateW,
                        'EsDate' => $EsDate,
                        'EPDate' => $EpDate,
                        'BalancePc' =>  (float)$item->BalancePc,
                        'S6' =>  (float)$item->S6,
                        'StoreBalance' =>  (float)$item->StoreBalance,
                        'SalePrice' =>  (float)$item->SalePrice,
                        'SaleValue' =>  (float)$item->SaleValue,
                        'idCompb' =>  $item->idCompb==""?$idCompany:$item->idCompb,
                        'idPd' =>  $item->idPd,
                        'SumWeight' =>  (float)$item->SumWeight
                    );
                    if($idCompany==Config::get('constants.Company.AVS.Id'))
                    {
                        $newResults[$results[$key]->PdName][Config::get('constants.Company.AVS.Name')] = $new_item;
                    }
                    else
                    {
                        $Company=$this->get_CompanyName($results[$key]->idCompb);
                        $newResults[$results[$key]->PdName][$Company]= $new_item;
                    }

                }
            }
        }
        if($idCompany<0||$idCompany==Config::get('constants.Company.AVS.Id'))
        {
            $weight = array();
            foreach ($newResults as $key => $row)
            {        
                $SumWeight =  0;
                $AvStock = 0;
                foreach ($row as $keys => $item)
                {
                    $SumWeight+=$item['BalancePc']+$item['S6']+$item['PsEstimate'];
                    $AvStock=$item['AvStock'];
                }
                $weight[$key] = $SumWeight+$AvStock;
            }
            array_multisort($weight, SORT_DESC, $newResults);
        }
        return $newResults;
    }
    public function report_stock_group_data($results,$idCompany)
    {
        $newResults = array();
        $step = array();
        $resultsDateEP_1=$this->getDateEP_idCompb1();
        $resultsDateEP_2=$this->getDateEP_idCompb2();
        foreach($results as $key => $item)
        {
            $sum=0;
            if($idCompany==Config::get('constants.Company.AVS.Id'))
            {
                $sum=$results[$key]->AvStock;
            }
            $sum+=$results[$key]->PsEstimate+$results[$key]->EstimateW+$results[$key]->BalancePc+$results[$key]->S6;
            if($results[$key]->PdName!='-'&&$results[$key]->PlantName!=null&&$sum!=0)
            {
                $MaxDateReceive1=$results[$key]->maxDateReceive_1;
                $MinDateReceive1=$results[$key]->minDateReceive_1;
                $MaxDateReceive2=$results[$key]->maxDateReceive_2;
                $MinDateReceive2=$results[$key]->minDateReceive_2;
                $EsDate=$this->get_dDataAllsum($MaxDateReceive1,$MinDateReceive1,$MaxDateReceive2,$MinDateReceive2);
                $EpDate = $this->getDateEP($results[$key]->idSeedType,$resultsDateEP_1,$resultsDateEP_2);
                $new_item = array(
                    'PdName' =>  $item->PdName,
                    'SeedTypeName' =>  $item->SeedTypeName,
                    'idSeed' =>  $item->idSeed,
                    'PlantName' =>  $item->PlantName,
                    'AvStock' =>  (float)$item->AvStock,
                    'PsEstimate' =>  (float)$item->PsEstimate,
                    'EstimateW' =>  (float)$item->EstimateW,
                    'EsDate' => $EsDate,
                    'EPDate' => $EpDate,
                    'BalancePc' =>  (float)$item->BalancePc,
                    'S6' =>  (float)$item->S6,
                    'StoreBalance' =>  (float)$item->StoreBalance,
                    'SalePrice' =>  (float)$item->SalePrice,
                    'SaleValue' =>  (float)$item->SaleValue,
                    'idCompb' =>  $item->idCompb==""?$idCompany:$item->idCompb,
                    'idPd' =>  $item->idPd,
                    'SumWeight' =>  (float)$item->SumWeight
                );
                if($idCompany==Config::get('constants.Company.AVS.Id'))
                {
                    $newResults[trim($results[$key]->PlantName)][trim($results[$key]->SeedTypeName)][trim($results[$key]->PdName)][Config::get('constants.Company.AVS.Name')] = $new_item;
                }
                else
                {
                    $Company=$this->get_CompanyName($results[$key]->idCompb);
                    $newResults[trim($results[$key]->PlantName)][trim($results[$key]->SeedTypeName)][trim($results[$key]->PdName)][$Company] = $new_item;
                }
            }
        }
        ksort($newResults);
        return $newResults;
    }
    public function getDateEP_idCompb1()
    {
        $sql = "SELECT idSeedType, S1, PcDry, PcBake, S2, S3, S4, Coating, S5 FROM dSeedTypeStoreManageDay Where idCompb = 1";
        $results1=DB::select($sql);
        return $results1;
    }
    public function getDateEP_idCompb2()
    {
        $sql = "SELECT idSeedType, S1, PcDry, PcBake, S2, S3, S4, Coating, S5 FROM dSeedTypeStoreManageDay Where idCompb = 2";
        $results2=DB::select($sql);
        return $results2;
    }
    public function getDateEP($idSeedType,$results1,$results2)
    {
        $max1=0;
        $min1=0;
        $max2=0;
        $min2=0;
        $SumDay=0;
        foreach ($results1 as $keys => $item)
        {
            if($item->idSeedType==$idSeedType)
            {
                $S5 = $item->S5;
                if($S5>0)
                {
                    if($S5>=1&&$S5<>0)
                    {
                        if($min1==0)
                        {
                            $min1 = $SumDay + 1;
                        } 
                    }
                    elseif($S5<1&&$S5==0)
                    {
                        $max1 = $SumDay + 1;
                    }
                }
                $SumDay++;
            }
        }
        $SumDay=0;
        foreach ($results2 as $keys => $item)
        {
            if($item->idSeedType==$idSeedType)
            {
                $S5 = $item->S5;
                if($S5>0)
                {
                    if($S5>=1&&$S5<>0)
                    {
                        if($min2==0)
                        {
                            $min2 = $SumDay + 1;
                        } 
                    }
                    elseif($S5<1&&$S5==0)
                    {
                        $max2 = $SumDay + 1;
                    }
                }
            }
        }


        if($min1==0&&$max1==0)
        {
            if($min2==$max2)
            {
                $DateEP=$max2;
            }
            else
            {
                $DateEP=$min2.' ถึง '.$max2;
            }
        }
        elseif($min2==0&&$max2==0)
        {
            if($min1==$max1)
            {
                $DateEP=$max1;
            }
            else
            {
                $DateEP=$min1.' ถึง '.$max1;
            }
        }
        else
        {
            if($min1<$min2&&$min1<$max2)
            {
                $DateEP=$min1.' ถึง ';
            }
            elseif($min2<$min1&&$min2<$max1)
            {
                $DateEP=$min2.' ถึง ';
            }
            else
            {
                $DateEP='';
            }
            if($max1>$max2)
            {
                $DateEP.=$max1;
            }
            else
            {
                $DateEP.=$max2;
            }
        }
        return $DateEP;

    }
    public function get_dDataAllsum($MaxDateReceive1,$MinDateReceive1,$MaxDateReceive2,$MinDateReceive2)
    {
        $date_now = STRTOTIME(DATE("Y-m-d H:i:s"));
        $ddMax1 = $this->Diff( (int) $MaxDateReceive1, $date_now); 
        $ddMin1 = $this->Diff( (int) $MinDateReceive1, $date_now); 
        if($ddMax1>$ddMin1)
        {
            $d1min = $ddMin1;
            $d1max = $ddMax1;
        }
        else
        {
            $d1min = $ddMax1;
            $d1max = $ddMin1;
        }
        $ddMax2 = $this->Diff( (int) $MaxDateReceive2, $date_now); 
        $ddMin2 = $this->Diff( (int) $MinDateReceive2, $date_now); 
        if($ddMax2>$ddMin2)
        {
            $d2min = $ddMin2;
            $d2max = $ddMax2;
        }
        else
        {
            $d2min = $ddMax2;
            $d2max = $ddMin2;
        }
        if($d1max==0&&$d1min==0)
        {
            if($d2max==$d2min)
            {
                $txtDate = $d2max;
            }
            else
            {
                $txtDate = $d2min.' ถึง '.$d2max;
            }
        }
        elseif($d2max==0&&$d2min==0)
        {
            if($d1max==$d1min)
            {
                $txtDate = $d1max;
            }
            else
            {
                $txtDate = $d1min.' ถึง '.$d1max;
            }
        }
        else
        {
            if($d1min<$d2min&&$d1min<$d2max)
            {
                $txtDate = $d1min.' ถึง ';
            }
            elseif($d2min<$d1min&&$d2min<$d1max)
            {
                $txtDate = $d2min.' ถึง ';
            }
            else
            {
                $txtDate = '';
            }
            if($d1max>$d2max)
            {
                $txtDate .= $d1max;
            }
            else
            {
                $txtDate .= $d2max;
            }
        }
        return $txtDate;
    }
    public function report_stock_main($idCompany,$sql_company,$sql_seed)
    {
        if($idCompany>0)
        {
            if($idCompany==Config::get('constants.Company.AVS.Id'))
            {
                $sql = "Select  ISNULL(c.maxDateReceive_1,0) as maxDateReceive_1 ,ISNULL(d.maxDateReceive_2,0) as maxDateReceive_2,
                ISNULL(c.minDateReceive_1,0) as minDateReceive_1 ,ISNULL(d.minDateReceive_2,0) as minDateReceive_2,v.idSeedType,
                v.idPd, v.PdName, v.SeedTypeName, SUM(Amount) As AvStock, v.SeedTypeName As PlantName,SUM(Amount) As S6,
                0.00 AS PsEstimate, 0.00 AS EstimateW, 0.00 as BalancePc, 
                0.00 as StoreBalance, 0.00 as SalePrice,0.00 as SaleValue, 
                '' as idCompb ,'' as idSeed,SUM(Amount) as SumWeight
                From vSeedProduct v INNER JOIN PkPdLotStock p ON v.idPd = p.idPd 
                left JOIN (SELECT idPd, max(DateReceive_) as maxDateReceive_1 , min(DateReceive_) as minDateReceive_1  FROM vPdIndexJobDt WHERE (stCancel IS NULL) AND (idCompb = 1) AND (idJobLine <> 0) AND (idJobLine <> 9) AND (idJobLine <> 10) group by idPd ) c on v.idpd = c.idpd 
			    left JOIN (SELECT idPd, max(DateReceive_) as maxDateReceive_2 ,min(DateReceive_) as minDateReceive_2 FROM vPdIndexJobDt WHERE (stCancel IS NULL) AND (idCompb = 2) AND (idJobLine <> 0) AND (idJobLine <> 9) AND (idJobLine <> 10) group by idPd ) d on v.idpd = d.idpd 
                ".$sql_seed." 
                GROUP BY v.idPd, v.PdName, v.SeedTypeName,v.idSeedType,c.maxDateReceive_1,c.minDateReceive_1,d.maxDateReceive_2,d.minDateReceive_2 ";
            }
            else
            {
                $sql = "SELECT Distinct  ISNULL(c.maxDateReceive_1,0) as maxDateReceive_1 ,ISNULL(d.maxDateReceive_2,0) as maxDateReceive_2,
                 ISNULL(c.minDateReceive_1,0) as minDateReceive_1 ,ISNULL(d.minDateReceive_2,0) as minDateReceive_2,vSsProductStoreBalance.idSeedType,
                vSsProductStoreBalance.PdName, vSsProductStoreBalance.SeedTypeName, vSsProductStoreBalance.idSeed,vSsProductStoreBalance.PlantName,
                ISNULL((SELECT SUM(Amount) FROM PkPdLotStock WHERE idPd = vSsProductStoreBalance.idPd),0.00) As AvStock, 
                ISNULL((SELECT SUM(PsEstimate) AS Expr1 FROM dbo.vPdIndexJobDt WHERE (stCancel IS NULL) AND (idPd = vSsProductStoreBalance.idPd)  AND (idSeedType = vSsProductStoreBalance.idSeedType) AND (idCompb = vSsProductStoreBalance.idCompb) And (idJobLine <> 0 And idJobLine <> 9  And idJobLine <> 10)),0.00) AS PsEstimate,
                ISNULL((SELECT SUM(EstimateW) AS Expr1 FROM dbo.vPdIndexJobDt WHERE (stCancel IS NULL) AND (idPd = vSsProductStoreBalance.idPd)  AND (idSeedType = vSsProductStoreBalance.idSeedType) AND (idCompb = vSsProductStoreBalance.idCompb) And (idJobLine <> 0 And idJobLine <> 9  And idJobLine <> 10)),0.00) AS EstimateW,
                ISNULL((isnull(S1NOW,0.00)+isnull(WIP1,0.00)+isnull(Bake,0.00)+isnull(S2,0.00)+isnull(S3,0.00)+isnull(S4,0.00)+isnull(WIP4,0.00)+isnull(S5,0.00)),0.00) as BalancePc, isnull(S6,0.00) as S6, 0.00 as StoreBalance,SalePrice,0.00 as SaleValue,vSsProductStoreBalance.idCompb, vSsProductStoreBalance.idPd 
                ,(isnull(S6,0.00) +(ISNULL((isnull(S1NOW,0.00)+isnull(WIP1,0.00)+isnull(Bake,0.00)+isnull(S2,0.00)+isnull(S3,0.00)+isnull(S4,0.00)+isnull(WIP4,0.00)+isnull(S5,0.00)),0.00))) as SumWeight
                FROM vSsProductStoreBalance 
                left JOIN (SELECT idPd, max(DateReceive_) as maxDateReceive_1 ,min(DateReceive_) as minDateReceive_1 FROM vPdIndexJobDt WHERE (stCancel IS NULL) AND (idCompb = 1) AND (idJobLine <> 0) AND (idJobLine <> 9) AND (idJobLine <> 10) group by idPd ) c on vSsProductStoreBalance.idpd = c.idpd 
			    left JOIN (SELECT idPd, max(DateReceive_) as maxDateReceive_2,min(DateReceive_) as minDateReceive_2 FROM vPdIndexJobDt WHERE (stCancel IS NULL) AND (idCompb = 2) AND (idJobLine <> 0) AND (idJobLine <> 9) AND (idJobLine <> 10) group by idPd ) d on vSsProductStoreBalance.idpd = d.idpd 
                ,vSeedproduct  
                WHERE ".$sql_company.$sql_seed." And vSeedproduct.idpd = vSsProductStoreBalance.idpd  
                And vSeedproduct.stdel is null ";
            }

        }
        else
        {
            $sql = "SELECT  ISNULL(c.maxDateReceive_1,0) as maxDateReceive_1 ,ISNULL(d.maxDateReceive_2,0) as maxDateReceive_2,
             ISNULL(c.minDateReceive_1,0) as minDateReceive_1 ,ISNULL(d.minDateReceive_2,0) as minDateReceive_2,
            b.* From
            (Select Distinct idPd,PdName,SeedTypeName From  vSsProductStoreBalance
                                            WHERE (isnull(S1NOW,0.00)+isnull(WIP1,0.00)+isnull(Bake,0.00)+isnull(S2,0.00)+isnull(S3,0.00)
                                                +isnull(S4,0.00)+isnull(WIP4,0.00)+isnull(S5,0.00)+isnull(S6,0.00) > 0)) a,
            (SELECT Distinct PdName, SeedTypeName, idSeed,PlantName,
            ISNULL((SELECT SUM(Amount) FROM PkPdLotStock WHERE idPd = vSsProductStoreBalance.idPd),0.00) As AvStock, 
            ISNULL((SELECT SUM(PsEstimate) AS Expr1 FROM dbo.vPdIndexJobDt WHERE (stCancel IS NULL) AND (idPd = vSsProductStoreBalance.idPd)  AND (idSeedType = vSsProductStoreBalance.idSeedType) AND (idCompb = vSsProductStoreBalance.idCompb) And (idJobLine <> 0 And idJobLine <> 9  And idJobLine <> 10)),0.00) AS PsEstimate,
            ISNULL((SELECT SUM(EstimateW) AS Expr1 FROM dbo.vPdIndexJobDt WHERE (stCancel IS NULL) AND (idPd = vSsProductStoreBalance.idPd)  AND (idSeedType = vSsProductStoreBalance.idSeedType) AND (idCompb = vSsProductStoreBalance.idCompb) And (idJobLine <> 0 And idJobLine <> 9  And idJobLine <> 10)),0.00) AS EstimateW,
            ISNULL((isnull(S1NOW,0.00)+isnull(WIP1,0.00)+isnull(Bake,0.00)+isnull(S2,0.00)+isnull(S3,0.00)+isnull(S4,0.00)+isnull(WIP4,0.00)+isnull(S5,0.00)),0.00) as BalancePc, isnull(S6,0.00) as S6, 0.00 as StoreBalance,SalePrice,0.00 as SaleValue,idCompb, idPd 
            ,((ISNULL((SELECT SUM(Amount) FROM PkPdLotStock WHERE idPd = vSsProductStoreBalance.idPd),0.00)+
                (ISNULL((SELECT SUM(PsEstimate) AS Expr1 FROM dbo.vPdIndexJobDt WHERE (stCancel IS NULL) 
                AND (idPd = vSsProductStoreBalance.idPd)  
                AND (idSeedType = vSsProductStoreBalance.idSeedType) 
                AND (idCompb = vSsProductStoreBalance.idCompb) 
                And (idJobLine <> 0 And idJobLine <> 9  
                And idJobLine <> 10)),0.00))
                +(isnull(S6,0.00)))
                +(ISNULL((isnull(S1NOW,0.00)+isnull(WIP1,0.00)+isnull(Bake,0.00)+isnull(S2,0.00)+isnull(S3,0.00)+isnull(S4,0.00)+isnull(WIP4,0.00)+isnull(S5,0.00)),0.00))) as SumWeight,
                idSeedType
            FROM vSsProductStoreBalance WHERE ".$sql_company.$sql_seed.") b left JOIN vSeedproduct vS ON vS.idpd = b.idpd 
            left JOIN (SELECT idPd, max(DateReceive_) as maxDateReceive_1 ,min(DateReceive_) as minDateReceive_1 FROM vPdIndexJobDt WHERE (stCancel IS NULL) AND (idCompb = 1) AND (idJobLine <> 0) AND (idJobLine <> 9) AND (idJobLine <> 10) group by idPd ) c on vS.idpd = c.idpd 
            left JOIN (SELECT idPd, max(DateReceive_) as maxDateReceive_2 ,min(DateReceive_) as minDateReceive_2 FROM vPdIndexJobDt WHERE (stCancel IS NULL) AND (idCompb = 2) AND (idJobLine <> 0) AND (idJobLine <> 9) AND (idJobLine <> 10) group by idPd ) d on vS.idpd = d.idpd 
            Where a.idPd=b.idPd and vS.stdel is null and b.SeedTypeName IS NOT NULL ";
        }
        return $sql;
    }
    public function report_stock_seed($idCompany,$idSeed,$idProduct)
    {
        $sql_seed = '';
        
        if(is_numeric($idSeed))
        {
            if($idCompany>0)
            {
                if($idCompany==Config::get('constants.Company.AVS.Id'))
                {
                    if(is_numeric($idProduct))
                    {
                        $sql_seed = " Where v.idPd=$idProduct ";
                    }
                    else
                    {
                        $sql_seed = " Where v.idSeed=$idSeed ";
                    }
                }
                else
                {
                    if(is_numeric($idProduct))
                    {
                        $sql_seed = " And vSsProductStoreBalance.idPd=$idProduct ";
                    }
                    else
                    {
                        $sql_seed = " And vSsProductStoreBalance.idSeed=$idSeed ";
                    }
                }
            }
            else
            {
                if(is_numeric($idProduct))
                {
                    $sql_seed = " And idPd=$idProduct ";
                }
                else
                {
                    $sql_seed = " And idSeed=$idSeed ";
                }
            }

        }
        return $sql_seed;
    }
    public function report_stock_company($idCompany)
    {
        $Company = Config::get('constants.Company');
        if($idCompany==$Company['ALLSPLIT']['Id'])
        {
            //แบบแยก
            $sql_company = "  (idCompb=1 OR idCompb=2 OR idCompb=8) ";
        }
        else
        {
            //แบบรวมและบริษัท vSsProductStoreBalance
            if($idCompany>0)
            {
                if($idCompany==$Company['ALLSUM']['Id'])
                {
                    $sql_company = "  (vSsProductStoreBalance.idCompb=1 OR vSsProductStoreBalance.idCompb=2 OR vSsProductStoreBalance.idCompb=8) " ;
                }
                elseif($idCompany==$Company['AVS']['Id'])
                {
                    $sql_company = " ";
                }
                else
                {
                    $sql_company = "  vSsProductStoreBalance.idCompb=$idCompany ";
                }
            }
            else
            {
                if($idCompany==$Company['ALLSUM']['Id'])
                {
                    $sql_company = "  (idCompb=1 OR idCompb=2 OR idCompb=8) " ;
                }
                else
                {
                    $sql_company = "  idCompb=$idCompany ";
                }
            }
        }
        return $sql_company;
    }
    public function report_stock_orderBy($idCompany,$order_by)
    {
        if($idCompany>0)
        {
            if($idCompany==Config::get('constants.Company.AVS.Id'))
            {
                $sql_order = " order by AvStock Desc ";
            }
            else
            {
                $sql_order = " order by vSsProductStoreBalance.PdName ASC";
                if($order_by=='weight')
                {
                    $sql_order = " order by SumWeight Desc";//น้ำหนัก
                }
            }

        }
        else
        {
            $sql_order = " order by b.PdName ASC";
            if($order_by=='weight')
            {
                $sql_order = " order by b.SumWeight Desc";//น้ำหนัก
            }
        }
        return $sql_order;
    }
/**************end report_stock***************/
/**************popup_stock_detail***************/
///*******PdEstPC********///
    public function PdEstPC($PdName,$PlantName,$idPd,$idcomp,$EsDate)
    {
        $data=array();
        $item=array();
        if($idcomp!=0)
        {
            $sql = $this->SQLPdEstPC($idPd,$idcomp);
            $result=DB::select($sql);  
            $array=$this->PdEstPC_MakeData($result,$idPd,$idcomp);
            array_push($item,$array);
        }
        else
        {
            $sql = $this->SQLPdEstPC($idPd,Config::get('constants.Company.FLOTECH.Id'));
            $result=DB::select($sql);  
            $FLOTECH_data=$this->PdEstPC_MakeData($result,$idPd,Config::get('constants.Company.FLOTECH.Id'));
            $sql = $this->SQLPdEstPC($idPd,Config::get('constants.Company.GREENSEEDS.Id'));
            $result=DB::select($sql);
            $GREENSEEDS_data=$this->PdEstPC_MakeData($result,$idPd,Config::get('constants.Company.GREENSEEDS.Id'));
            array_push($item,$FLOTECH_data,$GREENSEEDS_data);
        }
        $data=array(
            'idPd'=>$idPd,
            'PdName'=>$PdName,
            'PlantName'=>$PlantName,
            'EsDate'=>$EsDate,
            'item'=>$item
        );
        return view('reports.stock.modal.PdEstPC')->with([
            'data' => $data,
        ]);
    }
    public function PdEstPC_MakeData($result,$idPd,$idcomp)
    {
        $Company=$this->get_CompanyName($idcomp);
        $Sum = $result[0]->S1NOW + $result[0]->WIP1 + $result[0]->Bake+ $result[0]->S2 + $result[0]->S3 +$result[0]->S4 +$result[0]->WIP4 +$result[0]->S5;
        $rCol2_S1=(is_null($result[0]->Col2_S1)?0:$result[0]->Col2_S1);          
        $rCol2_PcDry=(is_null($result[0]->Col2_PcDry)?0:$result[0]->Col2_PcDry);         
        $rCol2_PcBake=(is_null($result[0]->Col2_PcBake)?0:$result[0]->Col2_PcBake);          
        $rCol2_S2=(is_null($result[0]->Col2_S2)?0:$result[0]->Col2_S2);          
        $rCol2_S3=(is_null($result[0]->Col2_S3)?0:$result[0]->Col2_S3);          
        $rCol2_S4=(is_null($result[0]->Col2_S4)?0:$result[0]->Col2_S4);         
        $rCol2_Coating=(is_null($result[0]->Col2_Coating)?0:$result[0]->Col2_Coating);         
        $rCol2_S5=(is_null($result[0]->Col2_S5)?0:$result[0]->Col2_S5);
        if($rCol2_S1==0||($result[0]->S1NOW/$rCol2_S1)==0)
        { 
            $Col2_S1 = 0;
        }
        else
        {
            if(($result[0]->S1NOW/$rCol2_S1)<0.05) 
            {
                $Col2_S1 = 0.1;
            }
            else
            {
                $Col2_S1 = $result[0]->S1NOW/$rCol2_S1;
            }
        }
        if($rCol2_PcDry==0||($result[0]->WIP1/$rCol2_PcDry)== 0)
        { 
            $Col2_PcDry = 0;
        }
        else
        {
            if(($result[0]->WIP1/$rCol2_PcDry)<0.0)
            {
                $Col2_PcDry = 0.1;
            }
            else
            {
                $Col2_PcDry = $result[0]->WIP1/$rCol2_PcDry;
            }
        }
        if($rCol2_PcBake==0||($result[0]->Bake/$rCol2_PcBake)==0) 
        {
            $Col2_PcBake = 0;
        }
        else
        {
            if(($result[0]->Bake/$rCol2_PcBake)< 0.05 )
            {
                $Col2_PcBake = 0.1;
            }
            else
            {
                $Col2_PcBake = $result[0]->Bake/$rCol2_PcBake;
            }
        }
        if($rCol2_S2==0||($result[0]->S2/$rCol2_S2)==0) 
        {
            $Col2_S2 = 0;
        }
        else
        {
            if(($result[0]->S2 / $rCol2_S2) < 0.05)
            { 
                $Col2_S2 = 0.1;
            }
            else
            {
                $Col2_S2 = ($result[0]->S2/$rCol2_S2);
            }
        }
        if($rCol2_S3==0||($result[0]->S3/$rCol2_S3)==0) 
        {
            $Col2_S3 = 0;
        }
        else
        {
            if(($result[0]->S3/$rCol2_S3)<0.05) 
            {
                $Col2_S3 = 0.1;
            }
            else
            {
                $Col2_S3 = $result[0]->S3/$rCol2_S3;
            }
        }
        if($rCol2_S4==0||($result[0]->S4/$rCol2_S4)==0) 
        {
            $Col2_S4 = 0;
        }
        else
        {
            if(($result[0]->S4/$rCol2_S4)<0.05) 
            {
                $Col2_S4 = 0.1;
            }
            else
            {
                $Col2_S4 = $result[0]->S4/$rCol2_S4;
            }
        }
        if($rCol2_Coating==0||($result[0]->WIP4/$rCol2_Coating)==0) 
        {
            $Col2_Coating = 0;
        }
        else
        {
            if(($result[0]->WIP4/$rCol2_Coating)<0.05)
            { 
                $Col2_Coating = 0.1;
            }
            else
            {
                $Col2_Coating = $result[0]->WIP4/$rCol2_Coating;
            }
        }
        if($rCol2_S5==0||($result[0]->S5/$rCol2_S5)==0) 
        {
            $Col2_S5 = 0;
        }
        else
            {
            if(($result[0]->S5/$rCol2_S5)<0.05)
            { 
                $Col2_S5 = 0.1;
            }
            else
            {
                $Col2_S5 = $result[0]->S5/$rCol2_S5;
            }
        }
        $SumDay=$this->PdEstPC_CalDay($result[0]->idSeedType,$idcomp);
        $data=array(
            'S1NOW'=>$result[0]->S1NOW,
            'WIP1'=>$result[0]->WIP1,
            'Bake'=>$result[0]->Bake,
            'S2'=>$result[0]->S2,
            'S3'=>$result[0]->S3,
            'S4'=>$result[0]->S4,
            'WIP4'=>$result[0]->WIP4,
            'S5'=>$result[0]->S5,
            'Col2_S1'=>$Col2_S1,  
            'Col2_PcDry'=>$Col2_PcDry,  
            'Col2_PcBake'=>$Col2_PcBake,  
            'Col2_S2'=>$Col2_S2,  
            'Col2_S3'=>$Col2_S3,  
            'Col2_S4'=>$Col2_S4,  
            'Col2_Coating'=>$Col2_Coating,  
            'Col2_S5'=>$Col2_S5,  
            'Col3_S1NOW'=>$result[0]->Col3_S1NOW,  
            'Col3_WIP1'=>$result[0]->Col3_WIP1, 
            'Col3_Bake'=>$result[0]->Col3_Bake, 
            'Col3_S2'=>$result[0]->Col3_S2,
            'Col3_S3'=>$result[0]->Col3_S3,
            'Col3_S4'=>$result[0]->Col3_S4,
            'Col3_WIP4'=>$result[0]->Col3_WIP4,
            'Col3_S5'=>$result[0]->Col3_S5,
            'Sum'=>$Sum,
            'idcomp'=>$idcomp,
            'idPd'=>$idPd,
            'SumDay'=>$SumDay,
            'Company'=>$Company
        );  
        return $data;      
    }
    public function PdEstPC_CalDay($idSeedType,$idcomp)
    {
        $max1=0;
        $min1=0;
        $SumDay=0;
        $txtDay="0 วัน";
        $sql=$this->SQLPdEstPC_Day($idSeedType,$idcomp);
        $result=DB::select($sql);
        if(count($result)>0)
        {
            foreach ($results as $keys => $item)
            {
                $S5 = $item->S5;
                if($S5>0)
                {
                    if($S5>=1&&$S5<>0)
                    {
                        if($min1==0)
                        {
                            $min1 = $SumDay + 1;
                        } 
                    }
                    elseif($S5<1&&$S5==0)
                    {
                        $max1 = $SumDay + 1;
                    }
                }
                $SumDay++;
            }
            if($max1==$min1)
            {
                if($max1 < 0.05)
                {
                    if($max1==0)
                    {
                        $txtDay = "0 วัน";
                    }
                    else
                    {
                        $txtDay = "0.1 หรือ 1 วัน";
                    }
                }
                else
                {
                    $txtDay = number_format($max1,2)." หรือ ".$max1." วัน";
                }
            }
            else
            {
                if($min1 < 0.05)
                {
                    if($max1==0.1)
                    {
                        $txtDay = "0.1 หรือ "; 
                        if($min1==$max1)
                        {
                            $txtDay .= $max1." วัน";
                        }
                        else
                        {
                            $txtDay .= $min1." ถึง ".$max1." วัน";
                        }
                    }
                    else
                    {
                        $txtDay = "0.1 ถึง ".number_format($max1,2)." หรือ ";
                        if($min1==$max1)
                        {
                            $txtDay .= $max1." วัน";
                        }
                        else
                        {
                            $txtDay .= $min1." ถึง ".$max1." วัน";
                        }
                    }
                }
                else
                {
                    if($min1==$max1)
                    {
                        $txtDay = number_format($max1,2)." หรือ ";
                    }
                    else
                    {
                        $txtDay = number_format($min1,2)." ถึง ".number_format($max1,2)." หรือ ";
                    }
                    if($min1==$max1)
                    {
                        $txtDay .= $max1." วัน";
                    }
                    else
                    {
                        $txtDay .= $min1." ถึง ".$max1." วัน";
                    }
                }
            }
            return $txtDay;
        }
        else
        {
            return $txtDay;
        }
    }
    public function SQLPdEstPC_Day($idSeedType,$idcomp)
    {
        $sql = "SELECT idSeedType, S1, PcDry, PcBake, S2, S3, S4, Coating, S5 
        FROM dSeedTypeStoreManageDay Where idSeedType = ".$idSeedType." AND idCompb = ".$idcomp;
        return $sql;
    }
    public function SQLPdEstPC($idPd,$idcomp)
    {
        $sql="select * from 
        (SELECT idPd,idSeedType,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE (idPd = " .$idPd. ") AND (idCompb = " .$idcomp. ") AND (idQcLine = '0') AND (idPsCancel IS NULL)),0.00) AS S1NOW,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE (idPd = " .$idPd. ") AND (idCompb = " .$idcomp. ") AND (idQcLine = 'T1') AND (idPsCancel IS NULL)),0.00) AS WIP1,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE (idPd = " .$idPd. ") AND (idCompb = " .$idcomp. ") AND (idQcLine = 'B1') AND (idPsCancel IS NULL)),0.00) AS Bake,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE (idPd = " .$idPd. ") AND (idCompb = " .$idcomp. ") AND (idQcLine = '2') AND (idPsCancel IS NULL)),0.00) AS S2,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE (idPd = " .$idPd. ") AND (idCompb = " .$idcomp. ") AND (idQcLine = '3' OR idQcLine = '3i' OR idQcLine = '3o') AND (idPsCancel IS NULL)),0.00) AS S3,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE (idPd = " .$idPd. ") AND (idCompb = " .$idcomp. ") AND (idQcLine = '4') AND (idPsCancel IS NULL)),0.00) AS S4,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE (idPd = " .$idPd. ") AND (idCompb = " .$idcomp. ") AND (idQcLine = '4w') AND (idPsCancel IS NULL)),0.00) AS WIP4,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE(idPd = " .$idPd. ") AND (idCompb = " .$idcomp. ") AND (idQcLine = '5') AND (idPsCancel IS NULL)),0.00) AS S5 , 
        idSeed 
        FROM vPdIndexJobRecPack_ WHERE idPd = " .$idPd. " GROUP BY idPd,idSeedType, idSeed)a
        left join 
        (SELECT ISNULL(idSeedType,0) as Col2_idSeedType, 
        ISNULL(S1,0.0) as Col2_S1 , 
        ISNULL(PcDry,0) as Col2_PcDry, 
        ISNULL(PcBake,0) as Col2_PcBake, 
        ISNULL(S2,0) as Col2_S2, 
        ISNULL(S3,0) as Col2_S3, 
        ISNULL(S4,0) as Col2_S4,
        ISNULL(Coating,0) as Col2_Coating, 
        ISNULL(S5,0)  as Col2_S5
        FROM dSeedTypeStoreManageDay Where idCompb = " .$idcomp. ")b 
        on a.idSeedType = b.Col2_idSeedType
        left join
        (SELECT idPd as Col3_idPd,idSeedType as Col3_idSeedType,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM  vPdIndexJobRecPack_ WHERE (idCompb = " .$idcomp. ") AND (idQcLine = '0') AND (idPsCancel IS NULL)),0.00) AS Col3_S1NOW,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_  WHERE (idCompb= " .$idcomp. ") AND (idQcLine = 'T1') AND (idPsCancel IS NULL)),0.00) AS Col3_WIP1,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM  vPdIndexJobRecPack_ WHERE (idCompb= " .$idcomp. ") AND (idQcLine = 'B1') AND (idPsCancel IS NULL)),0.00) AS Col3_Bake,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE (idCompb = " .$idcomp. ") AND (idQcLine = '2') AND (idPsCancel IS NULL)),0.00) AS Col3_S2,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE (idCompb = " .$idcomp. ") AND (idQcLine = '3' OR idQcLine = '3i' OR idQcLine = '3o') AND (idPsCancel IS NULL)),0.00) AS Col3_S3,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_ WHERE (idCompb = " .$idcomp. ") AND (idQcLine = '4') AND (idPsCancel IS NULL)),0.00) AS Col3_S4,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_  WHERE (idCompb = " .$idcomp. ") AND (idQcLine = '4w') AND (idPsCancel IS NULL)),0.00) AS Col3_WIP4,
        ISNULL((SELECT SUM(Kg_) AS Expr1 FROM vPdIndexJobRecPack_  WHERE (idCompb = " .$idcomp. ") AND (idQcLine = '5') AND (idPsCancel IS NULL)),0.00) AS Col3_S5
        FROM vPdIndexJobRecPack_ GROUP BY idPd,idSeedType) c 
        on a.idPd = c.Col3_idPd";
        return $sql;
    }
///*******End PdEstPC********///
///*******PdEstHavest********///
    public function PdEstHavest($PdName,$PlantName,$idPd,$idcomp,$EsDate,$sort_by,$sort_type,$list_type)
    {
        $data=array();
        $item=array();
        $order_by=$this->SQLEstHavest_OrderBy($sort_by,$sort_type);
        if($idcomp!=0)
        {
            $sql=$this->SQLEstHavest($idPd,$idcomp,$order_by);
            $result=DB::select($sql); 
            $array=$this->EstHavest_MakeData($result,$idPd,$idcomp,$list_type,$sort_by,$sort_type);
            array_push($item,$array);
        }
        else
        {
            $sql=$this->SQLEstHavest($idPd,Config::get('constants.Company.FLOTECH.Id'),$order_by);
            $result=DB::select($sql);
            $FLOTECH_data=$this->EstHavest_MakeData($result,$idPd,Config::get('constants.Company.FLOTECH.Id'),$list_type,$sort_by,$sort_type);
            $sql=$this->SQLEstHavest($idPd,Config::get('constants.Company.GREENSEEDS.Id'),$order_by);
            $result=DB::select($sql); 
            $GREENSEEDS_data=$this->EstHavest_MakeData($result,$idPd,Config::get('constants.Company.GREENSEEDS.Id'),$list_type,$sort_by,$sort_type);
            array_push($item,$FLOTECH_data,$GREENSEEDS_data); 
        }
        
        if($sort_by=="undefined")
        {
            $sort_by="duedate";
        }
        if($sort_type=="undefined")
        {
            $sort_type="asc";
        }
        if($list_type=="undefined")
        {
            $list_type="day";
        }
        $data=array(
            'idPd'=>$idPd,
            'PdName'=>$PdName,
            'PlantName'=>$PlantName,
            'EsDate'=>$EsDate,
            'idComp'=>$idcomp,
            'item'=>$item,
            'sort_by'=>$sort_by,
            'sort_type'=>$sort_type,
            'list_type'=>$list_type,
        );
        return view('reports.stock.modal.PdEstHavest')->with([
            'data' => $data,
        ]);
    }
    public function EstHavest_MakeData($result,$idPd,$idcomp,$list_type,$sort_by,$sort_type)
    {
        $Company=$this->get_CompanyName($idcomp);
        $data = array();
        $item = array();
        $sum = 0;
        $sumPs = 0;
        $txt_dif='0%';
        if(count($result)>0)
        {
            if($list_type=='month')
            {
                $month_year=array();
                $month_data=array();
                foreach ($result as $index=>$row)
                {
                    $sum += $row->EstimateW;
                    $sumPs += $row->PsEstimate;
                    $DateReceive_ = $this->StrToDateMonth($row->DateReceive_);
                    if (in_array($DateReceive_, $month_year))
                    {
                        $month_data[$DateReceive_]['PsEstimate']+=$row->PsEstimate;
                        $month_data[$DateReceive_]['EstimateW']+=$row->EstimateW;
                        $month_data[$DateReceive_]['col_date']+=$row->col_date;
                        $month_data[$DateReceive_]['DateReceive_']=$DateReceive_;
                        $month_data[$DateReceive_]['col_num1']+=$row->col_num1;
                    }
                    else
                    {
                        array_push($month_year,$DateReceive_); 
                        $month_data[$DateReceive_]['PsEstimate']=$row->PsEstimate;
                        $month_data[$DateReceive_]['EstimateW']=$row->EstimateW;
                        $month_data[$DateReceive_]['col_date']=$row->col_date;
                        $month_data[$DateReceive_]['DateReceive_']=$DateReceive_;
                        $month_data[$DateReceive_]['col_num1']=$row->col_num1;
                    }
                }
                $i=0;
                foreach ($month_data as $index=>$row)
                {
                    $item[$i] = array(
                        'col_num1'=>$row['col_num1'],
                        'DateReceive_'=>$row['DateReceive_'],
                        'col_date'=>$row['col_date'],
                        'PsEstimate'=>$row['PsEstimate'],
                        'EstimateW'=>$row['EstimateW'],
                    );
                    $i++;
                }
                if($sort_by=='weight')
                {
                    foreach ($item as $key => $row)
                    {
                        $EstimateW[$key] = (float)$row['EstimateW'];
                    }
                    if($sort_type=="asc")
                    {
                        array_multisort($EstimateW, SORT_ASC, $item);
                    }
                    else
                    {
                        array_multisort($EstimateW, SORT_DESC, $item);
                    }
                }
                if($sum==0&&$sumPs==0)
                {
                    $txt_dif = "0%";
                }
                else
                {
                    $dif = ($sumPs-$sum)/($sumPs+$sum);
                    $dif = $dif * 100;
                    if($dif<0)
                    {
                        $dif=$dif*(-1);
                    }
                    $txt_dif = number_format($dif,2)."%";
                }
                $data = array(
                    'idPd'=>$idPd,
                    'idcomp'=>$idcomp,
                    'Company'=>$Company,
                    'status'=>true,
                    'sum'=>$sum,
                    'sumPs'=>$sumPs,
                    'txt_dif'=>$txt_dif,
                    'items'=>$item
                );
            }
            else
            {
                foreach ($result as $index=>$row)
                {
                    $sum += $row->EstimateW;
                    $sumPs += $row->PsEstimate;
                    $DateReceive_ = $this->StrToDate($row->DateReceive_); 
                    $col_date=$this->Diff($row->DateReceive_);
                    $item[$index] = array(
                        'col_num1'=>$row->col_num1,
                        'DateReceive_'=>$DateReceive_,
                        'col_date'=>$col_date,
                        'PsEstimate'=>$row->PsEstimate,
                        'EstimateW'=>$row->EstimateW,
                    );
                }
                if($sum==0&&$sumPs==0)
                {
                    $txt_dif = "0%";
                }
                else
                {
                    $dif = ($sumPs-$sum)/($sumPs+$sum);
                    $dif = $dif * 100;
                    if($dif<0)
                    {
                        $dif=$dif*(-1);
                    }
                    $txt_dif = number_format($dif,2)."%";
                }
                $data = array(
                    'idPd'=>$idPd,
                    'idcomp'=>$idcomp,
                    'Company'=>$Company,
                    'status'=>true,
                    'sum'=>$sum,
                    'sumPs'=>$sumPs,
                    'txt_dif'=>$txt_dif,
                    'items'=>$item
                );
            }
        }
        else
        {
            $item[0] = array(
                'col_num1'=>0,
                'DateReceive_'=>0,
                'col_date'=>0,
                'PsEstimate'=>0,
                'EstimateW'=>0,
            );
            $data = array(
                'idPd'=>$idPd,
                'idcomp'=>$idcomp,
                'Company'=>$Company,
                'status'=>false,
                'sum'=>$sum,
                'sumPs'=>$sumPs,
                'txt_dif'=>$txt_dif,
                'items'=>$item
            );
        }
        return $data ;
    }
    public function SQLEstHavest_OrderBy($sort_by,$sort_type)
    {
        if($sort_by=="weight")
        {
            if($sort_type=="desc")
            {
                $order_by = "ORDER BY SUM(v.EstimateW) DESC";
            }
            else
            {
                $order_by = "ORDER BY SUM(v.EstimateW) ASC";
            }

        }
        else
        {
            if($sort_type=="desc")
            {
                $order_by = "ORDER BY v.DateReceive_ DESC";
            }
            else
            {
                $order_by = "ORDER BY v.DateReceive_ ASC";
            }
        }
        return $order_by;
    }
    public function SQLEstHavest($idPd,$idcomp,$order_by)
    {
        $sql="SELECT 0 As col_num1,v.DateReceive_,0 As col_date ,SUM(v.PsEstimate) AS PsEstimate ,
        SUM(v.EstimateW) AS EstimateW FROM vPdIndexJobDt v 
        INNER JOIN vSsProductStoreBalance s ON v.idPd=s.idPd  
        AND v.idSeedType = s.idSeedType AND v.idCompb = s.idCompb 
        WHERE (v.idPd = ".$idPd.") AND (v.stCancel IS NULL)  AND 
        (s.idCompb =".$idcomp.") AND (v.idJobLine <> 0 And v.idJobLine <> 9 And v.idJobLine <> 10)
         GROUP BY v.DateReceive_ ,v.EstimateW ".$order_by;
        return $sql;
    }
///*******End PdEstHavest********///
///*******PdLotMixList********///
    public function PdLotMixList(Request $request)
    {
        $input = $request->all();
        $PdName=$input['ProductName'];
        $PlantName=$input['PlantName'];
        $idPd=$input['ProductId'];
        $idcomp=$input['Company'];
        $Weight=$input['Weight'];
        $LotCode=$input['LotCode'];
        $Grad=$input['Grad'];
        $txtGrowS=$input['txtGrowS'];
        $txtGrowE=$input['txtGrowE'];
        $txtHumidS=$input['txtHumidS'];
        $txtHumidE=$input['txtHumidE'];
        $txtPureS=$input['txtPureS'];
        $txtPureE=$input['txtPureE'];
        $item=array();
        $Count=count($item);
        $ConvertIdcomp=$this->PdLotMixList_ConvertIdcomp($idcomp);
        $where=$this->SQLLotMixList_Chk($LotCode,$Grad);
        $sql=$this->SQLPdLotMixList($idPd,$ConvertIdcomp,$where);
        $result=DB::select($sql); 
        $data=$this->PdLotMixList_MakeData($result,$idPd,$idcomp,$PdName,$PlantName,$Weight,$txtGrowS,$txtGrowE,$txtHumidS,$txtHumidE,$txtPureS,$txtPureE);
        return view('reports.stock.modal.PdLotMixList')->with([
            'data' => $data,
        ]);
    }
    public function PdLotMixList_ConvertIdcomp($idcomp)
    {
        if($idcomp==Config::get('constants.Company.FLOTECH.Id'))
        {
            $CIdcomp=5;
        }
        elseif($idcomp==Config::get('constants.Company.GREENSEEDS.Id'))
        {
            $CIdcomp=3;
        }
        else
        {
            $CIdcomp=$idcomp;
        }
        return  $CIdcomp;
    }
    public function PdLotMixList_MakeData($result,$idPd,$idcomp,$PdName,$PlantName,$Weight,$txtGrowS,$txtGrowE,$txtHumidS,$txtHumidE,$txtPureS,$txtPureE)
    {
        $Company=$this->get_CompanyName($idcomp);
        $data = array();
        $item = array();
        $i = 0;
        $sumW = 0;
        if(count($result)>0)
        {
            foreach ($result as $index=>$row)
            {   
                if(is_null($txtGrowS)&&
                    is_null($txtGrowE)&&
                    is_null($txtHumidS)&&
                    is_null($txtHumidE)&&
                    is_null($txtPureS)&&
                    is_null($txtPureE))
                {
                    $item[$index] = array(
                        'idLot'=>$row->idLot,
                        'LotNum'=>$row->LotNum,
                        'LotCode'=>$row->LotCode,
                        'DateLot'=>$row->DateLot,
                        'SeasonName'=>$row->SeasonName,
                        'idProduct'=>$row->idProduct,
                        'PdName'=>$row->PdName,
                        'idPlantType'=>$row->idPlantType,
                        'SeedTypeName'=>$row->SeedTypeName,
                        'WUpdate'=>$row->WUpdate,
                        'Grad'=>$row->Grad,
                        'Grow'=>$row->Grow,
                        'Humid'=>$row->Humid,
                        'Pure'=>$row->Pure,
                        'ColNote'=>$row->Note,
                    );
                    $sumW+=$row->WUpdate;
                    $i++;
                }
                elseif((!is_null($txtGrowS)&&
                !is_null($txtGrowE))&&
                ((int)$txtGrowS<=(int)$txtGrowE)&&
                (int)$txtGrowS<=$row->Grow&&
                (int)$txtGrowE>=$row->Grow)
                {
                    $item[$index] = array(
                        'idLot'=>$row->idLot,
                        'LotNum'=>$row->LotNum,
                        'LotCode'=>$row->LotCode,
                        'DateLot'=>$row->DateLot,
                        'SeasonName'=>$row->SeasonName,
                        'idProduct'=>$row->idProduct,
                        'PdName'=>$row->PdName,
                        'idPlantType'=>$row->idPlantType,
                        'SeedTypeName'=>$row->SeedTypeName,
                        'WUpdate'=>$row->WUpdate,
                        'Grad'=>$row->Grad,
                        'Grow'=>$row->Grow,
                        'Humid'=>$row->Humid,
                        'Pure'=>$row->Pure,
                        'ColNote'=>$row->Note,
                    );
                    $sumW+=$row->WUpdate;
                    $i++;
                }
                elseif(!is_null(($txtHumidS)&&
                !is_null($txtHumidE))&&
                ((int)$txtHumidS<=(int)$txtHumidE)&&
                (int)$txtHumidS<=$row->Humid&&
                (int)$txtHumidE>=$row->Humid)
                {
                    $item[$index] = array(
                        'idLot'=>$row->idLot,
                        'LotNum'=>$row->LotNum,
                        'LotCode'=>$row->LotCode,
                        'DateLot'=>$row->DateLot,
                        'SeasonName'=>$row->SeasonName,
                        'idProduct'=>$row->idProduct,
                        'PdName'=>$row->PdName,
                        'idPlantType'=>$row->idPlantType,
                        'SeedTypeName'=>$row->SeedTypeName,
                        'WUpdate'=>$row->WUpdate,
                        'Grad'=>$row->Grad,
                        'Grow'=>$row->Grow,
                        'Humid'=>$row->Humid,
                        'Pure'=>$row->Pure,
                        'ColNote'=>$row->Note,
                    );
                    $sumW+=$row->WUpdate;
                    $i++;
                }
                elseif((!is_null($txtPureS)&&
                !is_null($txtPureE))&&
                ((int)$txtPureS<=(int)$txtPureE)&&
                (int)$txtPureS<=$row->Pure&&
                (int)$txtPureE>=$row->Pure)
                {
                    $item[$index] = array(
                        'idLot'=>$row->idLot,
                        'LotNum'=>$row->LotNum,
                        'LotCode'=>$row->LotCode,
                        'DateLot'=>$row->DateLot,
                        'SeasonName'=>$row->SeasonName,
                        'idProduct'=>$row->idProduct,
                        'PdName'=>$row->PdName,
                        'idPlantType'=>$row->idPlantType,
                        'SeedTypeName'=>$row->SeedTypeName,
                        'WUpdate'=>$row->WUpdate,
                        'Grad'=>$row->Grad,
                        'Grow'=>$row->Grow,
                        'Humid'=>$row->Humid,
                        'Pure'=>$row->Pure,
                        'ColNote'=>$row->Note,
                    );
                    $sumW+=$row->WUpdate;
                    $i++;
                }
            }
            $data = array(
                'idPd'=>$idPd,
                'idcomp'=>$idcomp,
                'Company'=>$Company,
                'PdName'=>$PdName,
                'PlantName'=>$PlantName,
                'status'=>true,
                'Weight'=>$sumW,
                'Count'=>$i,
                'items'=>$item
            );
        }
        else
        {
            $data = array(
                'idPd'=>$idPd,
                'idcomp'=>$idcomp,
                'Company'=>$Company,
                'PdName'=>$PdName,
                'PlantName'=>$PlantName,
                'status'=>true,
                'Weight'=>$Weight,
                'Count'=>0,
                'items'=>$item
            );
        }
        return $data;
    }
    public function SQLPdLotMixList($idPd,$idcomp,$where)
    {
        $sql="Select idLot, LotNum, LotCode, DateLot, SeasonName, idProduct, PdName, idPlantType, SeedTypeName, ".
        "WUpdate, Grad, Grow, Humid, Pure, Note, idComp, '' as Grow_, '' as Humid_, '' as Pure_ From vSsLotMt ".
        "Where idSale is Null And stStock is Null And idPsCancel is Null AND idProduct ='".$idPd."' ".
        "And idComp = '".$idcomp."'".$where." Order by PdName";
        return $sql;
    }
    public function SQLLotMixList_Chk($LotCode,$Grad)
    {
        $sql="";
        if(!is_null($LotCode)&&$LotCode!="0")
        {
            $sql.=" and LotCode = '".trim($LotCode)."'";
        }
        if(!is_null($Grad)&&$Grad!="0")
        {
            $sql.=" and Grad = '".trim(strtoupper($Grad))."'";
        }
        return $sql;
    }
///*******End PdLotMixList********///
///*******PdStock********///
    public function PdStock(Request $request)
    {
        $input = $request->all();
        $PdName=$input['ProductName'];
        $PlantName=$input['PlantName'];
        $idPd=$input['ProductId'];
        $idcomp=$input['Company'];
        $Weight=$input['Weight'];
        $LotCodeRef=$input['LotCodeRef'];
        $LotNum=$input['LotNum'];
        $Grad=$input['Grad'];
        $txtGrowS=$input['txtGrowS'];
        $txtGrowE=$input['txtGrowE'];
        $txtHumidS=$input['txtHumidS'];
        $txtHumidE=$input['txtHumidE'];
        $txtPureS=$input['txtPureS'];
        $txtPureE=$input['txtPureE'];
        $ChkNoPd=$input['ChkNoPd'];
        $ChkTab2=$input['Chk'];
        $idSeed=$input['idSeed'];
        $idProduct=$input['idProduct'];
        $where=$this->SQLPdStock_Chk($ChkNoPd,$LotCodeRef,$LotNum,$Grad);
        $sql=$this->SQLPdStock($idPd,$where);
        $result=DB::select($sql);
        $data=$this->PdStock_MakeData($result,$idPd,$idcomp,$PdName,$PlantName,$Weight,$txtGrowS,$txtGrowE,$txtHumidS,$txtHumidE,$txtPureS,$txtPureE);
        $ddSeed = $this->list_seed();
        $ddProduct = $this->list_seed_product(Config::get('constants.SeedId.Default'),null);

        $where=$this->SQLPdStockTab_Chk($idSeed,$idProduct,$ChkTab2);
        $sql=$this->SQLPdStockTab($where);
        $result=DB::select($sql);
        $tab=$this->PdStockTab2_MakeData($result);
        return view('reports.stock.modal.PdStock')->with([
            'data' => $data,
            'tab' =>$tab,
            'ddSeed' => $ddSeed,
            'ddProduct' => $ddProduct,
            'ChkTab2'=>$ChkTab2,
            'idSeed'=>$idSeed,
            'idProduct'=>$idProduct
        ]);
        
    }
    public function PdStock_MakeData($result,$idPd,$idcomp,$PlantName,$PdName,$Weight,$txtGrowS,$txtGrowE,$txtHumidS,$txtHumidE,$txtPureS,$txtPureE)
    {
        $Company=$this->get_CompanyName($idcomp);
        $data = array();
        $item = array();
        $i = 0;
        $sumW = 0;
        if(count($result)>0)
        {
            $DateRec=0;
            $sumAmountRec=0;
            $sumAmountUse=0;
            $sumAmount=0;
            foreach ($result as $index=>$row)
            {  ///////////////////////////////
                if(is_null($txtGrowS)&&
                is_null($txtGrowE)&&
                is_null($txtHumidS)&&
                is_null($txtHumidE)&&
                is_null($txtPureS)&&
                is_null($txtPureE))
                {
                    $i++;
                    $sumAmountRec+=$row->AmountRec;
                    $sumAmountUse+=$row->AmountUse;
                    $sumAmount+=$row->Amount;
                    if($DateRec<$row->DateRec)
                    {
                        $DateRec=$row->DateRec;
                    }
                    $item[$row->SeedTypeName][$row->PdName][$index] = array(
                        'DateRec'=>$this->StrToDate($row->DateRec),
                        'idPdLot'=>$row->idPdLot,
                        'LotNum'=>$row->LotNum,
                        'LotCodeRef'=>$row->LotCodeRef,
                        'idPd'=>$row->idPd,
                        'PdName'=>$row->PdName,
                        'SeedTypeName'=>$row->SeedTypeName,
                        'AmountRec'=>$row->AmountRec,
                        'AmountUse'=>$row->AmountUse,
                        'Amount'=>$row->Amount,
                        'Grad'=>$row->Grad,
                        'Grow'=>$row->Grow,
                        'Humid'=>$row->Humid,
                        'Pure'=>$row->Pure,
                        'DocPackCode'=>$row->DocPackCode,
                        'GrowRec'=>$row->GrowRec,
                        'HumidRec'=>$row->HumidRec,
                        'PureRec'=>$row->PureRec,
                        'GrowAA'=>$row->GrowAA,
                        'GrowTZ'=>$row->GrowTZ,
                        'GrowPaper'=>$row->GrowPaper,
                        'GrowSand'=>$row->GrowSand,
                        'Note'=>$row->Note,
                    );
                }
                elseif((!is_null($txtGrowS)&&
                !is_null($txtGrowE))&&
                ((int)$txtGrowS<=(int)$txtGrowE)&&
                (int)$txtGrowS<=$row->Grow&&
                (int)$txtGrowE>=$row->Grow)
                {
                    $i++;
                    $sumAmountRec+=$row->AmountRec;
                    $sumAmountUse+=$row->AmountUse;
                    $sumAmount+=$row->Amount;
                    if($DateRec<$row->DateRec)
                    {
                        $DateRec=$row->DateRec;
                    }
                    $item[$row->SeedTypeName][$row->PdName][$index] = array(
                        'DateRec'=>$this->StrToDate($row->DateRec),
                        'idPdLot'=>$row->idPdLot,
                        'LotNum'=>$row->LotNum,
                        'LotCodeRef'=>$row->LotCodeRef,
                        'idPd'=>$row->idPd,
                        'PdName'=>$row->PdName,
                        'SeedTypeName'=>$row->SeedTypeName,
                        'AmountRec'=>$row->AmountRec,
                        'AmountUse'=>$row->AmountUse,
                        'Amount'=>$row->Amount,
                        'Grad'=>$row->Grad,
                        'Grow'=>$row->Grow,
                        'Humid'=>$row->Humid,
                        'Pure'=>$row->Pure,
                        'DocPackCode'=>$row->DocPackCode,
                        'GrowRec'=>$row->GrowRec,
                        'HumidRec'=>$row->HumidRec,
                        'PureRec'=>$row->PureRec,
                        'GrowAA'=>$row->GrowAA,
                        'GrowTZ'=>$row->GrowTZ,
                        'GrowPaper'=>$row->GrowPaper,
                        'GrowSand'=>$row->GrowSand,
                        'Note'=>$row->Note,
                    );
                }
                elseif(!is_null(($txtHumidS)&&
                !is_null($txtHumidE))&&
                ((int)$txtHumidS<=(int)$txtHumidE)&&
                (int)$txtHumidS<=$row->Humid&&
                (int)$txtHumidE>=$row->Humid)
                {
                    $i++;
                    $sumAmountRec+=$row->AmountRec;
                    $sumAmountUse+=$row->AmountUse;
                    $sumAmount+=$row->Amount;
                    if($DateRec<$row->DateRec)
                    {
                        $DateRec=$row->DateRec;
                    }
                    $item[$row->SeedTypeName][$row->PdName][$index] = array(
                        'DateRec'=>$this->StrToDate($row->DateRec),
                        'idPdLot'=>$row->idPdLot,
                        'LotNum'=>$row->LotNum,
                        'LotCodeRef'=>$row->LotCodeRef,
                        'idPd'=>$row->idPd,
                        'PdName'=>$row->PdName,
                        'SeedTypeName'=>$row->SeedTypeName,
                        'AmountRec'=>$row->AmountRec,
                        'AmountUse'=>$row->AmountUse,
                        'Amount'=>$row->Amount,
                        'Grad'=>$row->Grad,
                        'Grow'=>$row->Grow,
                        'Humid'=>$row->Humid,
                        'Pure'=>$row->Pure,
                        'DocPackCode'=>$row->DocPackCode,
                        'GrowRec'=>$row->GrowRec,
                        'HumidRec'=>$row->HumidRec,
                        'PureRec'=>$row->PureRec,
                        'GrowAA'=>$row->GrowAA,
                        'GrowTZ'=>$row->GrowTZ,
                        'GrowPaper'=>$row->GrowPaper,
                        'GrowSand'=>$row->GrowSand,
                        'Note'=>$row->Note,
                    );
                }
                elseif((!is_null($txtPureS)&&
                !is_null($txtPureE))&&
                ((int)$txtPureS<=(int)$txtPureE)&&
                (int)$txtPureS<=$row->Pure&&
                (int)$txtPureE>=$row->Pure)
                {
                    $i++;
                    $sumAmountRec+=$row->AmountRec;
                    $sumAmountUse+=$row->AmountUse;
                    $sumAmount+=$row->Amount;
                    if($DateRec<$row->DateRec)
                    {
                        $DateRec=$row->DateRec;
                    }
                    $item[$row->SeedTypeName][$row->PdName][$index] = array(
                        'DateRec'=>$this->StrToDate($row->DateRec),
                        'idPdLot'=>$row->idPdLot,
                        'LotNum'=>$row->LotNum,
                        'LotCodeRef'=>$row->LotCodeRef,
                        'idPd'=>$row->idPd,
                        'PdName'=>$row->PdName,
                        'SeedTypeName'=>$row->SeedTypeName,
                        'AmountRec'=>$row->AmountRec,
                        'AmountUse'=>$row->AmountUse,
                        'Amount'=>$row->Amount,
                        'Grad'=>$row->Grad,
                        'Grow'=>$row->Grow,
                        'Humid'=>$row->Humid,
                        'Pure'=>$row->Pure,
                        'DocPackCode'=>$row->DocPackCode,
                        'GrowRec'=>$row->GrowRec,
                        'HumidRec'=>$row->HumidRec,
                        'PureRec'=>$row->PureRec,
                        'GrowAA'=>$row->GrowAA,
                        'GrowTZ'=>$row->GrowTZ,
                        'GrowPaper'=>$row->GrowPaper,
                        'GrowSand'=>$row->GrowSand,
                        'Note'=>$row->Note,
                    );
                } 
                
            }
            $data = array(
                'idPd'=>$idPd,
                'idcomp'=>$idcomp,
                'Company'=>$Company,
                'PdName'=>$PdName,
                'PlantName'=>$PlantName,
                'status'=>true,
                'sumAmountRec'=>$sumAmountRec,
                'sumAmountUse'=>$sumAmountUse,
                'sumAmount'=>$sumAmount,
                'DateRec'=> $this->StrToDate($DateRec),
                'Count'=>$i,
                'Weight'=>$sumAmount,
                'items'=>$item
            );
        }
        else
        {
            $data = array(
                'idPd'=>$idPd,
                'idcomp'=>$idcomp,
                'Company'=>$Company,
                'PdName'=>$PdName,
                'PlantName'=>$PlantName,
                'status'=>true,
                'sumAmountRec'=>0,
                'sumAmountUse'=>0,
                'sumAmount'=>0,
                'DateRec'=>"",
                'Count'=>0,
                'Weight'=>$Weight,
                'items'=>$item
            );
        }
        return $data;
    }
    public function SQLPdStock($idPd,$where)
    {
        $sql="Select s.DateRec, s.idPdLot, s.LotNum, s.LotCodeRef, s.idPd, p.PdName, p.SeedTypeName, 
        s.AmountRec, s.AmountUse, s.Amount, s.Grad, s.Grow, '' as Grow_, s.Humid, '' as Humid_, s.Pure, '' as Pure_,
        s.DocPackCode, s.GrowRec, s.HumidRec ,s.PureRec, s.GrowAA, s.GrowTZ, s.GrowPaper, s.GrowSand,
        s.Note From PkPdLotStock s LEFT OUTER JOIN vSeedProduct p 
        ON s.idPd = p.idPd Where s.idPd =".$idPd.$where." Order by p.SeedTypeName";
        return $sql;
    }
    public function SQLPdStock_Chk($ChkNoPd,$LotCodeRef,$LotNum,$Grad)
    {
        $sql="";
        if($ChkNoPd)
        {
            $sql.=" AND s.Amount > '0' ";
        }
        if(!is_null($LotCodeRef)&&$LotCodeRef!="0")
        {
            $sql.=" and s.LotCodeRef = '".trim($LotCodeRef)."'";
        }
        if(!is_null($LotNum)&&$LotNum!="0")
        {
            $sql.=" and s.LotNum = '".trim($LotNum)."'";
        }
        if(!is_null($Grad)&&$Grad!="0")
        {
            $sql.=" and s.Grad = '".trim(strtoupper($Grad))."'";
        }
        return $sql;
    }
    public function SQLPdStockTab($where)
    {
        $sql="SELECT idPd, PdName, (SELECT SUM(Amount) AS AmountSum 
        FROM dbo.PkPdLotStock WHERE (idPd = derivedtbl_1.idPd)) AS Amount , MinOld, MaxOld, idSeed, idSeedType,Note 
        FROM (SELECT   dS.idPd, dS.PdName, dS.MinPacking AS MinOld, dS.MaxPacking AS MaxOld, vS.idSeed, dS.idSeedType , '' AS Note 
        FROM dbo.dSeedProduct AS dS LEFT OUTER JOIN dbo.vSeedProduct AS vS ON dS.idPd = vS.idPd) AS derivedtbl_1  ".$where;
        return $sql;
    }
    public function SQLPdStockTab_Chk($idSeed,$idProduct,$Chk)
    {
        $sql=" WHERE 1=1 AND PdName <>  '-'  ";
        if(is_numeric($idSeed))
        {
            $sql.=" AND  idSeed = ".$idSeed;
        }
        if(is_numeric($idProduct))
        {
            $sql.=" AND  idSeedType = ".$idProduct;
        }
        if($Chk=="CHKFixMinMax")
        {
            $sql.=" AND  MinOld <> 0 AND MaxOld <> 0 ";
        }
        elseif($Chk=="CHKMin")
        {
            $sql.=" AND  MinOld <> 0 AND MaxOld <> 0 AND MinOld >= (SELECT SUM(Amount) AS AmountSum FROM dbo.PkPdLotStock WHERE (idPd = derivedtbl_1.idPd)) ";
        }
        elseif($Chk=="CHKMax")
        {
            $sql.=" AND  MinOld <> 0 AND MaxOld <> 0 AND MaxOld <= (SELECT SUM(Amount) AS AmountSum FROM dbo.PkPdLotStock WHERE (idPd = derivedtbl_1.idPd)) ";
        }
        else
        {
            $sql.="";
        }
        return $sql;
    }
    public function PdStockTab2_MakeData($result)
    {
        if(count($result)>0)
        {
            foreach ($result as $index=>$row)
            {
                $Note="";
                if($row->Amount<=$row->MinOld)
                {
                    $Note="มีจำนวนน้อยกว่ากำหนด";
                }
                if($row->Amount>=$row->MaxOld)
                {
                    $Note="มีจำนวนมากกว่ากำหนด";
                }
                if($row->MinOld==0&&$row->MaxOld==0)
                {
                    $Note="ยังไม่ได้กำหนดค่า";
                }
                $item[$index] = array(
                    'PdName'=>$row->PdName,
                    'Amount'=>$row->Amount,
                    'MaxOld'=>$row->MaxOld,
                    'MinOld'=>$row->MinOld,
                    'Note'=>$Note,
                );
            }           
        }
        else
        {
            $item = array();
        }
        return $item;
    }

///*******End PdStock********///

/**************end_stock_detail***************/
/**************public function***************/
    public function get_CompanyName($idCompb)
    {
        if($idCompb==Config::get('constants.Company.FLOTECH.Id'))
        {
            $Company=Config::get('constants.Company.FLOTECH.Name');
        }
        else if($idCompb==Config::get('constants.Company.GREENSEEDS.Id'))
        {
            $Company=Config::get('constants.Company.GREENSEEDS.Name');
        }
        else if($idCompb==Config::get('constants.Company.AVS.Id'))
        {
            $Company=Config::get('constants.Company.AVS.Name');
        }
        return $Company; 
    }
    public function Diff($start,$end = false) 
    { 
        if($start==0)
        {
            return 0;
        }
        else
        {
            if(!$end) { $end = time(); } 
            if(!is_numeric($start) || !is_numeric($end)) { return false; } 
            // Convert $start and $end into EN format (ISO 8601) 
            $start = date('Y-m-d H:i:s',$start); 
            $end = date('Y-m-d H:i:s',$end); 
            $d_start = date_create_from_format('Y-m-d H:i:s',$start); 
            $d_end = date_create_from_format('Y-m-d H:i:s',$end); 
            $diff = $d_start->diff($d_end); 
            // return all data 
            $this->year = $diff->format('%y'); 
            $this->month = $diff->format('%m'); 
            $this->day = $diff->format('%d'); 
            $this->hour = $diff->format('%h'); 
            $this->min = $diff->format('%i'); 
            $this->sec = $diff->format('%s'); 
            return $this->day ;
        }

    } 
    public function StrToDate($strDate)
    {
        $year=substr($strDate,0,4);
        $month=substr($strDate,4,2);
        $day=substr($strDate,6,2);
        $date=$day.'/'.$month.'/'.$year;
        return $date;
    }
    public function StrToDateMonth($strDate)
    {
        $year=substr($strDate,0,4);
        $month=substr($strDate,4,2);
        $day=substr($strDate,6,2);
        $date=$month.'/'.$year;
        return $date;
    }
    public function demo()
    {
        $ddSeed = $this->list_seed();
        $ddProduct = $this->list_seed_product(Config::get('constants.SeedId.Default'),null);
        $ddCompany = $this->list_company();
        $ddOrderBy = $this->list_orderBy();
        return view('reports.stock.rp_stock_demo')->with([
            'ddSeed' => $ddSeed,
            'ddProduct' => $ddProduct,
            'ddCompany' => $ddCompany,
            'ddOrderBy' => $ddOrderBy,
        ]);
    }
/**************end public function***************/
}
