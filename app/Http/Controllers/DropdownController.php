<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DropdownController extends Controller
{
    public function list_seed()
    {
        $options['-'] = " ------- ทั้งหมด ------ ";
        $seeds=DB::select("Select Distinct idSeed,PlantName From vSsProductStoreBalance Order By PlantName Asc");
        foreach($seeds as $seed_data)
        {
            if(isset($seed_data->idSeed))
            {
                $options[$seed_data->idSeed] = $seed_data->PlantName;
            }
        }
        return $options;
    }
    public function list_seed_product($idSeed,$type)
    {
        if(is_numeric($idSeed))
        {
            $sql ="Select Distinct idPd,PdName From vSsProductStoreBalance Where idSeed =".$idSeed." ORDER BY PdName ASC";
            $seedProducts=DB::select($sql);
            if($type!="ajax")
            {
                $options['-'] = " ------- ทั้งหมด ------ ";
                foreach($seedProducts as $product_data)
                {
                    if(isset($product_data->idPd)&&$product_data->PdName!="-")
                    {
                        $options[$product_data->idPd] = $product_data->PdName;
                    }
                }
                return $options;
            }
            else
            {
                return $seedProducts;
            }
        }
        else
        {
            $options = array();
            return $options;
        }


    }
    function list_company()
    {
        $Company = Config::get('constants.Company');
        $options[$Company['ALLSPLIT']['Id']] = $Company['ALLSPLIT']['Name'];
        $options[$Company['ALLSUM']['Id']] = $Company['ALLSUM']['Name'];
        $options[$Company['FLOTECH']['Id']] = $Company['FLOTECH']['Name'];
        $options[$Company['GREENSEEDS']['Id']] = $Company['GREENSEEDS']['Name'];
        $options[$Company['AVS']['Id']] = $Company['AVS']['Name'];
        return $options;
    }
    function list_orderBy()
    {
        $options['plant'] = 'ชื่อพืช';
        $options['weight'] = 'นน.ทั้งหมด';
        return $options;
    }
}
