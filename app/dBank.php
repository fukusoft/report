<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dBank extends Model
{
    public $timestamps = false;
    protected $table = 'dBank'; 
    protected $primaryKey = 'idBank'; 
    protected $fillable = ['idBank','BankCode','ShortName','BankName','stDel'];
}
