<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dSeedType extends Model
{
    public $timestamps = false;
    protected $table = 'dSeedType'; 
    protected $primaryKey = 'idSeedType'; 
    protected $fillable = ['idSeedType','idSeed','SeedTypeName','stDel'];
}
